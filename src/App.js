import { useState, useEffect } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Products from "./pages/Products";
import Signup from "./pages/Signup";
import Login from "./pages/Login";
import Logout from "./pages/Logout";
import Cart from "./pages/Cart";
import Checkout from "./pages/Checkout";
import Navbar from "./components/Navbar";
import Footer from "./components/Footer";
import Sidebar from "./components/admin/Sidebar";
import NavbarAdmin from "./components/admin/NavbarAdmin";
import Dashboard from "./pages/admin/Dashboard";
import AdminProducts from "./pages/admin/AdminProducts";
import AdminUsers from "./pages/admin/AdminUsers";
import Orders from "./pages/admin/Orders";
import BannerFooter from './components/home/BannerFooter';
import {AuthProvider} from './store/AuthContext';
import {Container, Wrapper } from "./Appstyles";
import UserProfile from "./pages/UserProfile";


const App = () => {

  const [user, setUser] = useState({
          id: null,
          isAdmin: null,
          image: null,
          username: null,
          firstName: null,
          lastName: null,
          email: null,
          mobileNo: null,
          bday: null,
          gender: null,
          password: null
    
  });

  const unsetUser = () => {
    localStorage.clear()
  }

  useEffect(()=>{

    fetch(`https://capstone-2-navera.onrender.com/users/details`,{
      headers:{
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res=>res.json())
    .then(data=>{

      console.log(data);

      if(typeof data._id !== "undefined"){

        setUser({
          id: data._id,
          isAdmin: data.isAdmin,
          image: data.image,
          username: data.username,
          firstName: data.firstName,
          lastName: data.lastName,
          email: data.email,
          mobileNo: data.mobileNo,
          bday: data.bday,
          gender: data.gender,
          password: data.password
        })
      }else{
        setUser({
          id: null,
          isAdmin: null,
          image: null,
          username: null,
          firstName: null,
          lastName: null,
          email: null,
          mobileNo: null,
          bday: null,
          gender: null,
          password: null
        })
      }
    })
  },[])



  return (
   
      
  <AuthProvider value={{ user, setUser, unsetUser }}>
    {(user.isAdmin === true)?
     <Router>
      <Container>
       <Sidebar/>
       <Wrapper>
          <NavbarAdmin/> 
          <Routes>
            <Route path="/admin/dashboard" element={<Dashboard/>}/>
            <Route path="/admin/products"  element={<AdminProducts/>}/> 
            <Route path="/admin/users"  element={<AdminUsers/>}/> 
            <Route path="/admin/orders"  element={<Orders/>}/>
            <Route path="/login"  element={<Login/>}/> 
            <Route path="/logout"  element={<Logout/>}/>
          </Routes>
        </Wrapper>
      </Container> 
    </Router>
  :
    <Router>
        <Navbar/>
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/products"  element={<Products/>}/>
            <Route path="/login"  element={<Login/>}/>
            <Route path="/cart"  element={<Cart/>}/>
            <Route path="/checkout"  element={<Checkout/>}/>
            <Route path="/profile"  element={<UserProfile/>}/>
            <Route path="/signup"  element={<Signup/>}/>
            <Route path="/logout"  element={<Logout/>}/>
          
          </Routes>
          <BannerFooter/>
          <Footer/> 
      </Router>
  
  }
    
  </AuthProvider> 
  );
};

export default App;
