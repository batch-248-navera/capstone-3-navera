const plugin = require('tailwindcss/plugin')
module.exports = {
  content: [   
  './src/**/*.{js,jsx,ts,tsx}',
  'node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}'
],
  theme: {
    extend: {},
  },
  plugins: [ 
    require('flowbite/plugin')
  ],
  plugins: [
    require("@tailwindcss/forms")({
      strategy: 'class', // only generate classes
    }),
    plugin(function ({ addUtilities }) {
      addUtilities({
        '.arrow-hide':{
          '&::-webkit-inner-spin-button':{
            '-webkit-appearance': 'none',
            'margin': 0
          },
          '&::-webkit-outer-spin-button':{
            '-webkit-appearance': 'none',
            'margin': 0
          },
        }
      }
      )
    })
  ]
}