import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    gap: 20px;
  
  
`

export const Foot1 = styled.div`
    display: flex;
    justify-content: center;
    gap: 5rem;
    flex-wrap: wrap;

    & li{
    
        list-style-type: none;
    }

`
export const Foot2 = styled.div`
    display: flex;
    flex-direction: column;
    gap: 1rem;
    align-items: center;

    & div{
        display: flex;
        gap: 10px;

        & span{
            font-size: 2rem;

            &:hover{
                color: #ee4d2d;
                cursor: pointer;
            }
        }
    }
`

