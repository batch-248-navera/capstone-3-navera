
import { NavLink as Link } from "react-router-dom";
import tw, {styled} from 'twin.macro'


export const Nav = styled.nav`
    ${tw` bg-black dark:bg-gray-900 fixed w-full z-20 top-0 left-0`}

    
`;
export const NavLink = styled(Link)`
  
`;
export const Button = styled.button`
  ${tw`inline-flex items-center p-2 ml-3 text-sm text-gray-500 rounded-lg md:hidden hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-gray-200 dark:text-gray-400 dark:hover:bg-gray-700 dark:focus:ring-gray-600`}
`;
export const MenuBar = styled.div`
  ${tw`hidden w-full md:block md:w-auto`}
`;
export const NavMenu = styled.ul`
 ${tw`font-medium flex flex-col p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-50 md:flex-row md:space-x-8 md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700`}
`;
export const Logo = styled(Link)`
  ${tw` flex text-white items-center`}
`;
export const User = styled.div`
  
`
 export const UserMenu = styled.div `
    

  
  
`;  

export const UserLink = styled(Link)`
     
      
      `

