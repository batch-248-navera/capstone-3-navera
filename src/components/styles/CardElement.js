import styled from "styled-components"

export const Container = styled.div`
    display: flex;
    justify-content: space-evenly;
    gap: 2px;
    flex-wrap: wrap;
`