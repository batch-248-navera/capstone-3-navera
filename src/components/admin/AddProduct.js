import { useState, useEffect, useContext } from "react";
import { Navigate, Link, useNavigate } from "react-router-dom";
import AuthContext from "../../store/AuthContext";
import img from "../../assets/photo.png";
import {
  Container,
  Wrapper,
  Buttons,
  ImagePreview,
} from "./styles/ManageProductStyles.js";
import {RiCloseFill} from 'react-icons/ri'

const AddProduct = ({onClose}) => {
  const { user } = useContext(AuthContext);

  const navigate = useNavigate();

  const [image, setImage] = useState(img);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [brand, setBrand] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);

  const [isActive, setIsActive] = useState(false);

  const handleProductImageUpload = (e) => {
    const file = e.target.files[0];

    TransformFileData(file);
  };

  const TransformFileData = (file) => {
    const reader = new FileReader();

    if (file) {
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        setImage(reader.result);
      };
    } else {
      setImage("");
    }
  };

  const addProduct = (e) => {
    e.preventDefault();

    fetch(`https://capstone-2-navera.onrender.com/products/add`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        image: image,
        name: name,
        description: description,
        brand: brand,
        category: category,
        price: price,
        stock: stock,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          alert("Product added successfully!");
          navigate("/admin/products");
        } else {
          alert("Failed!");
        }
      });

    setImage("");
    setName("");
    setDescription("");
    setBrand("");
    setCategory("");
    setPrice(0);
    setStock(0);
  };

  useEffect(() => {
    if (
      image !== "" &&
      name !== "" &&
      description !== "" &&
      brand !== "" &&
      category !== "" &&
      price > 0 &&
      stock > 0
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [image, name, description, brand, category, price, stock]);

  useEffect(() => {
    if (
      image !== "" &&
      name !== "" &&
      description !== "" &&
      brand !== "" &&
      category !== "" &&
      price > 0 &&
      stock > 0
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [image, name, description, brand, category, price, stock]);

  return (
    <Container>
      <Wrapper>
      <div className="header">
        <h3>Add Product</h3>
        <span onClick={onClose}><RiCloseFill/></span>
      </div>

      <form onSubmit={(e) => addProduct(e)}>
        <div className="split">
          <div className="left">
            <label> Product preview</label>
            <ImagePreview>
              <img src={image} alt="Product" />
            </ImagePreview>
            <label for="image">Product Image</label>
            <input
              id="uploadImg"
              type="file"
              accept="image/*"
              onChange={handleProductImageUpload}
            />
          </div>
          <div className="right">
            <label>Product name</label>
            <input
              type="text"
              value={name}
              onChange={(e) => setName(e.target.value)}
              required
            />
            <label>Description</label>
            <input
              type="text"
              value={description}
              onChange={(e) => setDescription(e.target.value)}
              required
            />

            <div>
              <div className="price">
                <label>Price</label>
                <input
                  type="number"
                  onChange={(e) => setPrice(e.target.value)}
                  required
                />
              </div>
              <div className="stock">
                <label>Stock</label>
                <input
                  type="number"
                  onChange={(e) => setStock(e.target.value)}
                  required
                />
              </div>
            </div>

            <select
              name="brands"
              id="brands"
              value={brand}
              onChange={(e) => setBrand(e.target.value)}
            >
              <option value="">Select Brand</option>
              <option value="nike">Nike</option>
              <option value="adidas">Adidas</option>
              <option value="reebok">Reebok</option>
              <option value="under_armour">Under Armour</option>
              <option value="fila">Fila</option>
            </select>

            <select
              name="category"
              id="category"
              value={category}
              onChange={(e) => setCategory(e.target.value)}
            >
              <option value="">Select Category</option>
              <option value="men">Men</option>
              <option value="women">women</option>
              <option value="kids">Kids</option>
            </select>
          </div>
        </div>

        <Buttons>
          {isActive ? (
            <button type="submit">Add Product</button>
          ) : (
            <button type="submit" disabled>
              Add Product
            </button>
          )}

          <button onClick={onClose}> Cancel </button>
        </Buttons>
      </form>
      </Wrapper>
    </Container>
  );
};

export default AddProduct;
