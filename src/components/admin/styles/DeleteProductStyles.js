import styled from "styled-components";


export const Container = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
    backdrop-filter: blur(2px);

`

export const Wrapper = styled.div`
    display: flex;
    flex-direction: column;
    justify-content: center;
    border-radius: 10px;
    padding: 4px 8px;
    background-color: white;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);


    & span:nth-child(1){
        align-self: flex-end;
        font-size: 2em;
        
        & :hover{
            cursor: pointer;
            color: red;
        }
    }

    & span:nth-child(2){
        align-self: center;
        font-size: 2rem;
        color: red;
    }

    & div{
        display: flex;
        align-self: flex-end;
        gap: 4px;

        & button{
            background-color: black;
            padding: 2px 4px;
            border-radius: 5px;
            color: white;
        }
    }
`