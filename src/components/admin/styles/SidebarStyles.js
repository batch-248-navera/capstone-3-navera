import styled from "styled-components";
import {Link} from "react-router-dom"



export const Logo = styled.div`
    visibility: hidden;

`


export const Profile = styled.div`
    display: flex;
    align-items: center;
    flex-direction: column;
    justify-content: space-between;
    padding:  10px 5px 12px 10px ;
    border-bottom: 1px solid gray;

    & span{
       font-size: 20px;
       color: white;

    }
`

export const NavMenu = styled.ul`
    display: flex;
    align-items: center;
    flex-direction: column;
    margin-top: 20px;
    padding:0 10px;
    height: 100%;
   
   & :last-child{
        align-content: flex-end;
    }

`

export const NavLink = styled(Link)`
    display: flex;
    align-items: center;
    color: white;
    gap: 10px;
    text-decoration: none;
    padding: 10px;
    border-radius: 10px ;

    & span{
        font-size: 1rem;

    }
    
    & li{
        
        display: none;
    }

    &:hover{
        color: black;
        background-color: white;
        cursor: pointer;

        & span{
        color: black;
        
    }
    }

    
    `
export const Nav = styled.div`
    
width: 5%;
height: 100vh;
background-color: black;
transition: width .2s ease-out ;

:hover{
    width: 20%;

    ${NavMenu}{
        align-items: flex-start;
        
    }

    ${Logo}{
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: nowrap;
        width: auto;
        color: white;
    }

    ${NavLink}{ 
    
    width: 100%;
    
    & li{
        
        display: block;
    }

    &:hover{
        color: black;
        background-color: white;
        cursor: pointer;

        & span{
        color: black;
        
    }
    

    }
}

}

`