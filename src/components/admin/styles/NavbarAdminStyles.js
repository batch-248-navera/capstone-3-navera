import styled from "styled-components";


export const Container = styled.div`
    position: relative;
    display: flex;
    
    justify-content: space-between;
    padding: 10px 5px;
    box-shadow: 0 0 10px 0 grey;
    background-color: white;
`
export const Left = styled.div`
    display: flex;
    align-items: center;
    color: black;
    border: 1px solid gray;
    border-radius: 10px;
    padding: 2px 4px;


    
    `
export const Right = styled.div`
    display: flex;
    align-items: center;
    gap: 20px;

    & div{
        display: flex;
        align-items: center;
        
        & :first-child{
            margin-right: 5px;
        }
    }
`

