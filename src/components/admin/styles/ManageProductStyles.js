import styled from "styled-components";
import {Link} from 'react-router-dom'

export const Container = styled.div`
  
  position: absolute;
    top: 0;
    left: 0;
    display: flex;
    width: 100%;
    height: 100%;
    justify-content: center;
    align-items: center;
    backdrop-filter: blur(5px);
    z-index: 10;
`
export const Wrapper = styled.div`
  
  
    display: flex;
    flex-direction: column;
    justify-content: center;
    border-radius: 10px;
    padding: 4px 8px;
    background-color: white;
    box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);


  & .header {
    display: flex;
    justify-content: center;
    width: 100%;
    justify-content: space-between;
    margin: 0;

    
  }

  & form {
    display: flex;
    flex-direction: column;
    align-content: center;
    justify-content: center;
    width: 100%;
    gap: 1em;
    margin: 0;

    & label{
        font-weight: bold;
    }
    

    & input {
      border: 1px solid grey;
    }

    & .split {
      display: flex;
      justify-content: space-around;
      align-items: flex-start;
      gap: 4rem;
      width: 100%;
      margin: 0;

      & .left {
        display: flex;
        flex-direction: column;
        
        

        & input{

            font-size: 12px;
            
        &::file-selector-button {
           background-color: black;
                color: white;

            &:hover{
                cursor: pointer;
            }
        }
        }
      
      }
    & .right {
        display: flex;
        flex-direction: column;

        & input, & select  {
          border: 1px solid grey;
          width: 400px;
          margin-bottom: 1em;
        }

        & div{
          display: flex;
          justify-content: space-between;

           & div{
            display: flex;
            flex-direction: column;
            
            & input{
              width: 150px;
            }
           }
        }

        
      }
    }
 }
`;

export const Buttons = styled.div`
      display: flex;
      justify-self: flex-end;
      align-self: end;
      gap: 10px;
`
export const Button = styled(Link)`
        background-color: black;
        color: white;
        padding: 2px 8px;

`;

export const ImagePreview = styled.div`
  display: flex;
  justify-content: center;
  width: 300px;
  height: 200px;
  border: 1px dashed grey;
`;
