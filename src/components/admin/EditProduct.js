import { useState, useEffect, useContext } from "react";
import { Navigate, Link, useNavigate} from "react-router-dom";
import AuthContext from "../../store/AuthContext";
import {
  Container,
  Wrapper,
  Buttons,
  ImagePreview,
} from "./styles/ManageProductStyles.js";
import { RiCloseFill } from "react-icons/ri";

const EditProduct = ({onClose, productId}) => {
  const { user } = useContext(AuthContext);

  const navigate = useNavigate();
 


  const [image, setImage] = useState("");
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [brand, setBrand] = useState("");
  const [category, setCategory] = useState("");
  const [price, setPrice] = useState(0);
  const [stock, setStock] = useState(0);

  const [isActive, setIsActive] = useState(false);
  const [submitButton, setSubmitButton] = useState("");

  const handleProductImageUpload = (e) => {
    const file = e.target.files[0];

    TransformFileData(file);
  };

  const TransformFileData = (file) => {
    const reader = new FileReader();

    if (file) {
      reader.readAsDataURL(file);
      reader.onloadend = () => {
        setImage(reader.result);
      };
    } else {
      setImage("");
    }
  };


  useEffect(() => {
    if (
      image !== "" &&
      name !== "" &&
      description !== "" &&
      brand !== "" &&
      category !== "" &&
      price > 0 &&
      stock > 0
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [image, name, description, brand, category, price, stock]);



  const editProduct = (e) => {
    e.preventDefault();

    fetch(`https://capstone-2-navera.onrender.com/products/update/${productId}`, {
      method: "PATCH",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        image: image,
        name: name,
        description: description,
        brand: brand,
        category: category,
        price: price,
        stock: stock,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          alert("Product updated successfully!");
        } else {
          alert("Failed!");
        }
      });

    setImage("");
    setName("");
    setDescription("");
    setBrand("");
    setCategory("");
    setPrice(0);
    setStock(0);
  };

  useEffect(() => {
    

    fetch(`https://capstone-2-navera.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);

        setImage(data.image);
        setName(data.name);
        setDescription(data.description);
        setBrand(data.brand);
        setCategory(data.category);
        setPrice(data.price);
        setStock(data.stock);
      });
  }, [productId]);

  useEffect(() => {
    if (
      image !== "" &&
      name !== "" &&
      description !== "" &&
      brand !== "" &&
      category !== "" &&
      price > 0 &&
      stock > 0
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [image, name, description, brand, category, price, stock]);

  return (
    <Container>
      <Wrapper>
        <div className="header">
          <h3>Edit Product</h3>
          <span onClick={onClose}><RiCloseFill/></span>
      
        </div>

        <form
          onSubmit={ (e) => editProduct(e)} >
          <div className="split">
            <div className="left">
              <label> Product preview</label>
              <ImagePreview>
                <img src={image} alt="Product" />
              </ImagePreview>
              <label for="image">Product Image</label>
              <input
                id="uploadImg"
                type="file"
                accept="image/*"
                onChange={handleProductImageUpload}
                
              />
            </div>
            <div className="right">
              <label>Product name</label>
              <input
                type="text"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
              <label>Description</label>
              <input
                type="text"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
              <label>Price</label>
              <input
                type="number"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
                
              <label>Stock</label>
              <input
                type="number"
                value={stock}
                onChange={(e) => setStock(e.target.value)}
                required
              />
              <select
                name="brands"
                id="brands"
                value={brand}
                onChange={(e) => setBrand(e.target.value)}
              >
                <option value="">Select Brand</option>
                <option value="nike">Nike</option>
                <option value="adidas">Adidas</option>
                <option value="reebok">Reebok</option>
                <option value="under_armour">Under Armour</option>
                <option value="fila">Fila</option>
              </select>

              <select
                name="category"
                id="category"
                value={category}
                onChange={(e) => setCategory(e.target.value)}
              >
                <option value="">Select Category</option>
                <option value="men">Men</option>
                <option value="women">women</option>
                <option value="kids">Kids</option>
              </select>
            </div>
          </div>

          <Buttons>
           
                {isActive ? (
                  <button type="submit">Edit Product</button>
                ) : (
                  <button type="submit" disabled>
                    Edit Product
                  </button>
                )}
              
                 <button onClick={onClose}> Cancel </button>
          </Buttons>
        </form>
        </Wrapper>
    </Container>
  );
};

export default EditProduct;
