import { useEffect, useContext, useState } from "react";
import { FaTrashAlt } from "react-icons/fa";
import {  RiCloseFill } from "react-icons/ri";
import {Container, Wrapper} from "./styles/DeleteProductStyles";

function DeleteProduct({onClose, productId}) {

    const [image, setImage] = useState("");

    useEffect(() => {
    

        fetch(`https://capstone-2-navera.onrender.com/products/${productId}`)
          .then((res) => res.json())
          .then((data) => {
            
            setImage(data.image);
          });
      }, [productId]);

    const deleteProduct = () =>{


    fetch(`https://capstone-2-navera.onrender.com/products/${productId}/delete`, {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      
    })
      .then((res) => res.json())
      .then((data) => {
        if (data) {
          alert("Product deleted successfully!");
          onClose();
        } else {
          alert("Failed!");
        }
      });

    
 
    }
      
  return (

    
    <Container>
      <Wrapper>
        <span onClick={onClose}><RiCloseFill/></span>
        <span>
          <FaTrashAlt />
        </span>
        <p>Are you sure you want to remove this product?</p>
        <div>
            <button onClick={deleteProduct}>Yes</button>
            <button onClick={onClose}>Cancel</button>   
        </div>
        
      </Wrapper>
    </Container>
  );
}

export default DeleteProduct;
