import { useState, useContext } from "react";
import AuthContext from "../../store/AuthContext";
import { Nav, Logo, NavMenu, NavLink, Profile } from "./styles/SidebarStyles";
import { Link } from "react-router-dom";
import { MdInventory2} from "react-icons/md";
import { FaUserCircle } from "react-icons/fa";
import { RiLogoutBoxLine } from "react-icons/ri";
import { DiAtom } from "react-icons/di";

import {
  RiDashboardFill,
  RiShoppingCart2Fill,
  RiTeamFill,
  RiSettings3Fill,
} from "react-icons/ri";

const Sidebar = () => {
  const { user } = useContext(AuthContext);

  return (
    <Nav className="h-1000">
      <Logo>
        <div as={Link} to="/admin/dashboard">
          <span>
            <DiAtom />
          </span>
          Shoeniverse
        </div>
      </Logo>

      <Profile>
        <span>
          <FaUserCircle />
        </span>

        <span>{user.username}</span>
      </Profile>

      <NavMenu>
       
        <NavLink to="/admin/dashboard">
          <span>
            <RiDashboardFill />
          </span>
          <li>Dashboard</li>
        </NavLink>
        <NavLink to="/admin/products">
          <span>
            <MdInventory2 />
          </span>
          <li>Products</li>
        </NavLink>
        <NavLink to="/admin/users">
          <span>
            <RiTeamFill />
          </span>
          <li>Users</li>
        </NavLink>
        <NavLink to="/admin/orders">
          <span>
            <RiShoppingCart2Fill />
          </span>
          <li>Orders</li>
        </NavLink>
        <NavLink to="/admin/settings">
          <span>
            <RiSettings3Fill />
          </span>
          <li>Settings</li>
        </NavLink>
        <NavLink to="/logout">
        <span>
          <RiLogoutBoxLine />
        </span>
        <li>Logout</li>
        </NavLink>
      </NavMenu>

      
    </Nav>
  );
};

export default Sidebar;
