import React, { useState } from "react";
import { useAsyncDebounce } from "react-table";
import styled from "styled-components";

const SearchContainer = styled.div`
  margin: 6px 0;
  display:flex;
  align-items: center;
`;

const SearchText = styled.h2`
  font-size: x-large;
  color: grey;
  margin-right: 6px;
`;

const Input = styled.input`
 border: 1px solid green;
padding: 4px;
 border-radius: 5px;
`;

export function GlobalFilter({
  preGlobalFilteredRows,
  globalFilter,
  setGlobalFilter,
}) {
  const count = preGlobalFilteredRows.length;
  const [value, setValue] = useState(globalFilter);
  const onChange = useAsyncDebounce((value) => {
    setGlobalFilter(value || undefined);
  }, 300);

  return (
    <SearchContainer>
      <SearchText>Search:</SearchText>
      <Input
        value={value || ""}
        onChange={(e) => {
          setValue(e.target.value);
          onChange(e.target.value);
        }}
        placeholder={`${count} records...`}
      />
    </SearchContainer>
  );
}