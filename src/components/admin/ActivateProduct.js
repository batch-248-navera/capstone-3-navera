import {useState, useEffect} from "react";
import {Container, Wrapper} from "./styles/DeleteProductStyles";
import {  RiCloseFill } from "react-icons/ri";


const ActivateProduct = ({onClose, productId, isActive}) => {


  console.log(productId, isActive)
  useEffect(() => {
    

    fetch(`https://capstone-2-navera.onrender.com/products/${productId}`)
      .then((res) => res.json())
      .then((data) => {
        
       
      });
  }, [productId]);



const activateProduct = () =>{


fetch(`https://capstone-2-navera.onrender.com/products/${productId}/unarchive`, {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${localStorage.getItem("token")}`,
  }
})
  .then((res) => res.json())
  .then((data) => {
    if (data) {
      alert("Product unarchived successfully!");
      onClose();
    } else {
      alert("Failed!");
    }
  });



}
  
const deactivateProduct = () =>{


fetch(`https://capstone-2-navera.onrender.com/products/${productId}/archive`, {
  method: "PATCH",
  headers: {
    "Content-Type": "application/json",
    Authorization: `Bearer ${localStorage.getItem("token")}`,
  },
  body: JSON.stringify({
    isActive:(!isActive)
  })
  ,
})
  .then((res) => res.json())
  .then((data) => {
    if (data) {
      alert("Product archived successfully!");
      onClose();
    } else {
      alert("Failed!");
    }
  });



}
  


  
  return (
    <Container>
      <Wrapper>
        <span onClick={onClose}><RiCloseFill/></span>
       
        <p>Are you sure you want to make this product {isActive?'unavailable':'available'}?</p>
        <div>
            <button onClick={isActive?deactivateProduct:activateProduct}>Yes</button>
            <button onClick={onClose}>Cancel</button>   
        </div>
        
      </Wrapper>
    </Container>
   
  )
}

export default ActivateProduct;