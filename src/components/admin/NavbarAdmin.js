  
import { useState, useContext } from "react";
import AuthContext from "../../store/AuthContext";
import {Link} from 'react-router-dom';
import { FaRegBell, FaAngleDown, FaSearch, FaUserCircle} from "react-icons/fa";
import {Container, Left, Right} from "./styles/NavbarAdminStyles";
import { TbMessageCircle } from "react-icons/tb";


const Header = () =>{
    const {user} = useContext(AuthContext);

    return (
        
        <Container>
            
            <Left>
                
                <input type='search'/>
                <span><FaSearch/></span>

            </Left>
            <Right>
                <span><TbMessageCircle/></span>
                <span><FaRegBell/></span>
                <div>
                    <span><FaUserCircle /></span>
                    <span>{`Welcome, ${user.username}`}</span>
                    <span><FaAngleDown/></span>
                </div>

            </Right>

        </Container>
        
    );
}

export default Header;