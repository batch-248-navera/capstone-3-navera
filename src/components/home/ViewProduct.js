import { useState,useEffect } from "react";
import {RiCloseFill} from 'react-icons/ri';
import Cart from '../../pages/Cart';

const ViewProduct=({onClose, productId})=>{

    const [image, setImage] = useState("");
    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    
    console.log(productId)
    useEffect(() => {
    

        fetch(`https://capstone-2-navera.onrender.com/products/${productId}`)
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
    
            setImage(data.image);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
           
          });
      }, [productId]);
    

      const addtoCart = (productId) =>{
		console.log(productId)
		fetch(`https://capstone-2-navera.onrender.com/products/${productId}/addToCart`, {
			method: "POST",
			headers: {
			  "Content-Type": "application/json",
			  Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				productId: productId,
				quantity: 1
			}),
		  })
			.then((res) => res.json())
			.then((data) => {
				
			  if (data) {
				
				alert("Product added to cart successfully!");
				
			  } else {
				alert("Failed!");
			  }
			});

	}
    
      return (
        <div className="w-full absolute left-0 top-0 flex justify-center items-center backdrop-filter-2 ">
          <div className="w-40 h-40 flex justify-center rounded-lg shadow-sm bg-white">
            <div className="">
              <h3>Edit Product</h3>
              <span onClick={onClose}><RiCloseFill/></span>
          
            </div>
    
            <div>
                <button onClick={()=>{
                addtoCart(productId)}}>Add To Cart</button>
                <button onClick={onClose}>Cancel</button>   
             </div>
          
            </div>
        </div>
      );

    

  }

export default ViewProduct;
