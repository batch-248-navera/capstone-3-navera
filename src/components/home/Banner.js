import { useState, useEffect } from "react";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";
import { RxDotFilled } from 'react-icons/rx';
import { BsArrowRight } from 'react-icons/bs';
import { Link } from "react-router-dom";

const Banner = () =>{

  const slides = [
    {
      url: "https://i.pinimg.com/originals/a6/d1/9d/a6d19da7e67c02f82371143dbb12e102.jpg"
    },
     
    {
      url: "https://about.underarmour.com/content/ua/about/en/stories/2021/10/stephen-curry-and-under-armour-to-drop--street-pack--collection-/jcr:content/root/container/image.coreimg.82.1200.jpeg",
     },
    {
      url: "https://mir-s3-cdn-cf.behance.net/projects/404/d48ac4162749845.63da950b10476.png",
      },
    {
      url: "https://cdn.dribbble.com/users/156148/screenshots/5988697/newui-dribbble.png",
      },
    {
      url: "https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/3943ff73405597.5c0839d3881e6.png",
      }
      ,
    {
      url: "https://image-cdn.hypb.st/https%3A%2F%2Fhypebeast.com%2Fimage%2F2020%2F07%2Fnike-zoom-freak-2-giannis-antetokounmpo-official-release-date-info-1.jpg",
      }
  ];

           
  const [currentIndex, setCurrentIndex] = useState(0);
  const autoScroll = true;
  let slideInterval;
  let intervalTime = 10000;

  function auto() {
    slideInterval = setInterval(nextSlide, intervalTime);
  }

  useEffect(() => {
    setCurrentIndex(0);
  },[]);

  useEffect(() => {
    if (autoScroll) {
      auto();
    }
    return () => clearInterval(slideInterval);
  }, [currentIndex]);



  const prevSlide = () => {
    const isFirstSlide = currentIndex === 0;
    const newIndex = isFirstSlide ? slides.length - 1 : currentIndex - 1;
    setCurrentIndex(newIndex);
  };

  const nextSlide = () => {
    const isLastSlide = currentIndex === slides.length - 1;
    const newIndex = isLastSlide ? 0 : currentIndex + 1;
    setCurrentIndex(newIndex);
  };

  const goToSlide = (slideIndex) => {
    setCurrentIndex(slideIndex);
  };

  return (
    <div className='max-w-[1400px] h-[400px] w-full  relative group '>
      <div
        style={{ backgroundImage: `url(${slides[currentIndex].url})` }}
        className='w-full h-full  bg-center bg-cover duration-500 p-0'
      ></div>
      
      <div className='hidden group-hover:block absolute top-[50%] -translate-x-0 translate-y-[-50%] left-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer'>
        <FaAngleLeft onClick={prevSlide} size={30} />
      </div>
      
      <div  className='hidden group-hover:block absolute top-[50%] -translate-x-0 translate-y-[-50%] right-5 text-2xl rounded-full p-2 bg-black/20 text-white cursor-pointer'>
        <FaAngleRight onClick={nextSlide} size={30} />
      </div>
      <Link to="/products" className="absolute flex items-center gap-2 top-[70%] left-[20%] z-10 border-2 p-2 border-white text-white hover:bg-black hover:text-black">
            <button className=" text-xl ">Shop now</button>
            <BsArrowRight size={20}  />
      </Link>
      <div className='flex  justify-center pb-2'>
        {slides.map((slide, slideIndex) => (
          <div
            key={slideIndex}
            onClick={() => goToSlide(slideIndex)}
            className='text-2xl cursor-pointer'
          >
           
          </div>
        ))}
      </div>
    </div>
  );
}



export default Banner;