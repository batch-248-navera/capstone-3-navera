import {Card,Image} from './styled-components/CategoryElements';
import ImgMens from '../../assets/category_mens.jpg' 
import ImgWomens from '../../assets/category_womens.jpg' 
import ImgKids from '../../assets/category_kids.jpg' 



const FeaturedCollections = () =>{

    const categories = [
                {
                    img:ImgMens,
                    label: "Mens"
                },
            
                {
                    img:ImgWomens,
                    label: "Womens"
                },
            
                {
                    img:ImgKids,
                    label: "Kids"
                },
        
            ];

            const categoriesMap = categories.map((category, index)=>{
            
              return (
                    <Image  backgroundImg={category.img} key={index}>
                        
                        <h3>{category.label}</h3>
                    </Image>
                ); 
           } )
          

    return (
       
                <Card>{categoriesMap}</Card>
          
            
    );

}

export default FeaturedCollections;