import {FaMoneyBillAlt, FaWallet, FaRegEnvelope} from 'react-icons/fa';
import { Container } from './styled-components/BannerFooterElements';

const BannerFoot = () =>{

       

    return(
        <Container>
              <div>
                 <span><FaWallet/></span>
                 <h5>Free Shipping Nationwide</h5>
                 <p>Save up on shipping for a minimum purchase of P5,000.</p>
              </div>
              <div>
                 <span><FaMoneyBillAlt/></span>
                 <h5>Free Shipping Nationwide</h5>
                 <p>Save up on shipping for a minimum purchase of P5,000.</p>
              </div>
              <div>
                 <span><FaRegEnvelope/></span>
                 <h5>Free Shipping Nationwide</h5>
                 <p>Save up on shipping for a minimum purchase of P5,000.</p>
              </div>
              
        </Container>
    );

}

export default BannerFoot;