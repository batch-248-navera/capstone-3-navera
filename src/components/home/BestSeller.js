import {useState, useEffect} from 'react';
import { productData } from "../image-slider/data";
import ProductCard from "../products/ProductCard";
import { Container} from "../styles/CardElement";

const BestSeller = () => {

  const [activeProducts, setActiveProducts] = useState([]);


  const fetchActiveProducts = () =>{
		fetch(`https://capstone-2-navera.onrender.com/products/active`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			
			
    setActiveProducts(data.map((product) => (
    
        <ProductCard
          key={product._id}
          productId={product._id}
          name={product.name}
          image={product.image}
          price={product.price}
          description={product.description}
        />
      
    ))
    )
		
								
			
		})
	}

	
	

	useEffect(()=>{
		fetchActiveProducts();
	},[])

  return <div className='grid place-items-center position sm:grid-cols-2 md:grid-cols-4 '>{activeProducts}</div>;
};

export default BestSeller;
