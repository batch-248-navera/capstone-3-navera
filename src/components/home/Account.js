import {Container} from "./AccountStyles.js"
import {FaRegUserCircle} from "react-icons/fa";

function Account() {
  return (
        <Container>
            <span><FaRegUserCircle/></span>
            <span>Username</span>
        </Container>
  )
}

export default Account