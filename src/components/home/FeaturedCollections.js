import {Card, Image} from './styled-components/FeaturedCollectionsElements';
import ImgJordan from '../../assets/featured_jordan.jpg' 
import ImgCurry from '../../assets/featured_curry.jpg' 
import ImgKobe from '../../assets/featured_kobe.jpg' 
import ImgLebron from '../../assets/featured_lebron.jpg' 



const FeaturedCollections = () =>{

    const featuredShoes = [
                {
                    img:ImgJordan,
                    label: "Nike Air Jordan"
                },
            
                {
                    img:ImgCurry,
                    label: "Under Armor | Curry"
                },
            
                {
                    img:ImgKobe,
                    label: "Black Mamba"
                },
            
                {
                    img:ImgLebron,
                    label: "King James"
                },
            
               
            ];

            const featured = featuredShoes.map((featured, index)=>{
            
              return (
                    <Image backgroundImg={featured.img} key={index}>
                         <h3>{featured.label}</h3>
                    </Image>
                ); 
           } )
          

    return (

                <Card>{featured}</Card>
          
            
    );

}

export default FeaturedCollections;