import { useState } from "react";
import {RiCloseLine} from "react-icons/ri"

export default function ProfileModal(props) {
    
    const [update, setUpdatedGender] = useState("")
    
    return (
      <div className="fixed flex justify-center items-center top-0 left-0 w-screen h-screen z-10 backdrop-blur-sm ">
        <div className="flex flex-col p-2 w-[300px] h-[150px] shadow-lg rounded-xl border-1 border-red-500 bg-white z-20 ">
        
        <span className="self-end text-2xl hover:cursor-pointer hover:text-red-500" onClick={props.onClose} ><RiCloseLine/></span>
          {props.update === "username" &&
          <div>

            <div  className="flex justify-center items-center gap-6">
                <label for="username" className="block mb-1 text-sm font-medium text-gray-900">Username:</label>
                
                 <input 
                 onChange={(e)=>setUpdatedGender(e.target.value)} 
                 type="text"
                 value="" 
                 name="gender" 
                 className=""/>
              
            </div>
             
            <div className="flex gap-2 justify-self-end">
              <button onClick={()=>{
                props.setGender(update)
                props.onClose()
              }} className="text-white text-sm bg-blue-500 p-1 rounded-lg"  >OK</button>
              <button onClick={props.onClose}  className="text-white text-sm bg-red-500 p-1 rounded-lg" >Cancel</button>  
            </div>
              
          </div>
          

            }

{props.update === "gender" &&
          <div>

            <div  className="flex justify-center items-center gap-6">
                <label for="gender" className="block mb-1 text-sm font-medium text-gray-900">Gender:</label>
                <div className="flex gap-6 my-2" >
                
                <div className="flex items-center mr-4">
                    <input onChange={(e)=>setUpdatedGender(e.target.value)} type="radio" value="Male" name="gender" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
                    <label  for="inline-radio" className="ml-2 text-sm font-medium text-gray-900 ">Male</label>
                </div>
                <div className="flex items-center mr-4">
                    <input onChange={(e)=>setUpdatedGender(e.target.value)}  type="radio" value="Female" name="gender" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
                    <label for="inline-2-radio" className="ml-2 text-sm font-medium text-gray-900 ">Female</label>
                </div>
                </div>
            </div>
             
            <div className="flex gap-2 justify-self-end">
              <button onClick={()=>{
                props.setGender(update)
                props.onClose()
              }} className="text-white text-sm bg-blue-500 p-1 rounded-lg"  >OK</button>
              <button onClick={props.onClose}  className="text-white text-sm bg-red-500 p-1 rounded-lg" >Cancel</button>  
            </div>
              
          </div>
          

            }
           
        </div>
        
      </div>
    );
  }