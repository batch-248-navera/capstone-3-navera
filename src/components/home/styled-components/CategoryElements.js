import styled from "styled-components";

export const Card = styled.div`
    display:flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
    
   
    
`
export const Image = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;
    width: 30%;
    height: 400px;
    background-image: url(${(props)=> props.backgroundImg});
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    transition: all 0.2s ease-in-out;
    margin-bottom: 2rem;

    & h3{
        color: white;
        
    }

    &:hover{
        width: 32%;
        height: 400px;
        cursor: pointer;
       
    }
`
