import styled from "styled-components";

export const Carousel = styled.div`
    position: relative;
    width: 100%;
    height: 500px;
`
export const Img = styled.img`
    position: absolute;
    top: 0;
    left: 0;
    opacity: 0;
    transition: opacity 0.5s ease-in-out;
    &:active{
       opacity:1;
    
    }
 `    
