import styled from "styled-components";

export const Card = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  gap: 3rem;
`;
export const Image = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 10px;
  width: 200px;
  height: 300px;
  background-image: url(${(props) => props.backgroundImg});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  transition: all 0.2s ease-in-out;
  margin-bottom: 2rem;



  & h3 {
    color: white;
  }

  &:hover {
    width: 220px;
    height: 320px;
    cursor: pointer;
    margin-top: -10px;

    @media (max-width: 768px) {
      width: 250px;
      height: 350px;
    }
  }
`;
