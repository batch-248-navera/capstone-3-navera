import styled from "styled-components";


export const Container = styled.div`
    display: flex;
    justify-content: center;
    gap: 5rem;
    flex-wrap: wrap;
    margin: 4rem 0;
    padding-top: 3rem;
    border-top: 1px solid gray;

    & div{
        display: flex;
        flex-direction: column;
        justify-content: center;
        align-items: center;

        & span{
            font-size: 3rem;
        }
    }

`

