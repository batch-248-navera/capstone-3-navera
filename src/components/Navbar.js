import { useState, useContext } from "react";
import { Link } from "react-router-dom";
import {RiMenu3Fill, RiCloseFill } from 'react-icons/ri';
import {DiAtom} from 'react-icons/di';
import {FaRegUserCircle, FaShoppingCart, FaAngleDown} from 'react-icons/fa';
import AuthContext from '../store/AuthContext';
import avatar from '../assets/man.png';

const Navbar = () =>{

    const { user } = useContext(AuthContext);
	


    const [navMenu, setnavMenu] = useState(false);
    const [userMenu, setuserMenu] = useState(false);

    
  
    return (
      
      <nav className='bg-black sticky top-0 left-0 z-40 flex justify-between items-center h-20 max-w-full mx-auto px-3 text-white'>
        <Link to='/' className='flex items-center text-xl md:text-2xl font-bold hover:text-white' >
            <span><DiAtom size={25}/></span>
            Shoeniverse
        </Link>
        <div className="flex md:flex-2 items-center">
        <ul className='hidden  md:flex items-center gap-6'>
          <Link to='/' className='pb-2 hover:border-b-2 border-white hover:text-white'>Home</Link>
          <Link to='/products' className='pb-2 hover:border-b-2 border-white hover:text-white'>Products</Link>
          {user.id===null&&
          <>
            <Link to='/signup' className='pb-2 hover:border-b-2 border-white hover:text-white'>Sign up</Link>
            <Link to='/login' className='pb-2 hover:border-b-2 border-white hover:text-white'>Login</Link>
          </>
         }
        </ul>
        {user.id!==null&&
        <div className="flex md:flex-1 flex-row">
        <Link to="/cart" className="relative inline-flex items-center p-3 text-sm font-medium text-center text-white rounded-lg focus:ring-4 focus:outline-none focus:ring-blue-300">
              <FaShoppingCart size={20} />
              <div class="absolute inline-flex items-center justify-center w-6 h-6 text-xs font-bold text-white bg-red-500 border-2 border-white rounded-full -top-2 -right-2 dark:border-gray-900">20</div>
            </Link>
            <div onClick={ () => {setuserMenu(!userMenu) }} className="flex gap-1 items-center hover:cursor-pointer">
              <img class="w-8 h-8 rounded-full" src={avatar} alt="Rounded avatar"/>
              <span>{user.username}</span>
              <span><FaAngleDown/></span>
            </div>
            <ul className={userMenu ? 'absolute  flex flex-col right-0 top-20 z-0 w-[200px] h-30 border-2 border-gray-400 bg-white ease-in-out duration-700 text-center' : ' hidden '}>
            
              <Link onClick={ () => {setuserMenu(!userMenu) }}  to='/profile' className='p-1 text-black w-full hover:bg-gray-200'>My Profile</Link>
              <Link onClick={ () => {setuserMenu(!userMenu) }}  to='/orders' className='p-1 text-black w-full hover:bg-gray-200'>My Orders</Link>
              <Link onClick={ () => {setuserMenu(!userMenu) }}  to='/logout' className='p-1 text-black w-full hover:bg-gray-200'>Logout</Link>
        
           </ul>
        </div>
        }
        </div>
        
        <div onClick={ () => {setnavMenu(!navMenu) }} className='block md:hidden'>
            {navMenu ? <RiCloseFill size={25}/> : <RiMenu3Fill size={25} />}
        </div>
        <ul className={navMenu ? 'lg:hidden md:hidden fixed flex flex-col right-0 top-20 text-center w-[50%] h-full border-r z-20 border-r-gray-900 bg-[#000300] ease-in-out duration-700' : ' flex flex-col ease-in-out duration-700 h-full fixed top-0 right-[-100%]'}>
            <Link onClick={ () => {setnavMenu(!navMenu) }}  to='/' className='p-4 border-b border-gray-600 cursor-pointer'>Home</Link>
            <Link onClick={ () => {setnavMenu(!navMenu) }} to='/products' className='p-4 border-b border-gray-600'>Products</Link>
            {user.id===null&&
            <>
             <Link onClick={ () => {setnavMenu(!navMenu) }} to='/signup' className='p-4 border-b border-gray-600'>Sign up</Link>
             <Link onClick={ () => {setnavMenu(!navMenu) }} to='/login' className='p-4 border-b border-gray-600'>Login</Link>
            </>
            }
        </ul>
      </nav>
    );
  };


export default Navbar;

