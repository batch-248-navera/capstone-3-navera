import { Container, Foot1, Foot2} from "./styles/FooterElements";
import { AiFillTwitterCircle, AiFillFacebook, AiFillInstagram } from 'react-icons/ai';

const Footer = () => {
  return (
    <Container>
      <Foot1>
      
        <ul>
          <h5>Terms and Conditions</h5>
          <li>Payment Method</li>
          <li>Cancelaltion Policy</li>
          <li>Privacy Policy</li>
          <li>Shipping Policy</li>
          <li>Return,Refund and Exchange Policy</li>
          <li>Terms of Use</li>
        </ul>
        
        <ul>
          <h5>Get Help</h5>
          <li>Order status</li>
          <li>Delivery</li>
          <li>Returns</li>
          <li>Payment Options</li>
          <li>Order Tracking</li>
          <li>Contact Us</li>
        </ul>
        
        <div>
          <h5>Connect</h5>
          <p>Join our mailing li for updates</p>
          <input type="email" placeholder="Enter email address"></input>
          <button type="submit">Join</button>
        </div>
        
      </Foot1>

      <Foot2>
        <div>
          <span><AiFillFacebook/></span>
          <span> <AiFillTwitterCircle/></span>
          <span><AiFillInstagram/></span>
        </div>
        <span>Copyright © 2023 Shoepee</span>
      </Foot2>
    </Container>
  );
};

export default Footer;
