import { useContext, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import AuthContext from "../../store/AuthContext";
import ProductCard from "./ProductCard";


const DisplayProducts = () => {

  const {user} = useContext(AuthContext);

	
	
	
	const [activeProducts, setActiveProducts] = useState([]);

	const navigate = useNavigate();

	

	const fetchActiveProducts = () =>{
		fetch(`https://capstone-2-navera.onrender.com/products/active`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			
			
    setActiveProducts(data.map((product) => (
    
        <ProductCard
          key={product._id}
          productId={product._id}
          name={product.name}
          image={product.image}
          price={product.price}
          description={product.description}
        />
      
    ))
    )
		
								
			
		})
	}

	
	

	useEffect(()=>{
		fetchActiveProducts();
	},[])
 

  return <div className="grid place-items-center sm:grid-cols-2 md:grid-cols-4 lg:grid-cols-4 pb-2 gap-2 m-4 ">{activeProducts}</div>;
};

export default DisplayProducts;
