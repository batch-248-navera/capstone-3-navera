import styled from "styled-components";

export const Card = styled.div`
  position: relative;
  text-align: center;
  width: 200px;
  height: auto;
  margin: 0.5em;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);

  @media (max-width: 768px) {
    width: 150px;
    height: 180px;
  }
  & img {
    width: 100%;
    height: 10em;
    object-fit: cover;
  }

  & div {
    background-color: white;
    height: auto;

    @media (max-width: 768px) {
      width: auto;
      height: 100px;
    }

    & .name {
      font-size: 1em;
      font-weight: bold;
    }

    & .price {
      color: grey;
      font-size: 0.8em;
    }

    & .description {
      margin: auto 1rem;
      font-size: 0.8em;
      text-align: center;
    }

    & button {
      position: absolute;
      top: 30%;
      left: 50%;
      transform: translate(-50%, -50%);
      border: none;
      outline: 0;
      padding: 12px;
      color: white;
      background-color: #000;
      text-align: center;
      width: auto;
      font-size: 12px;
      opacity: 0;
      transition: opacity 0.3s ease-out 0.2s;

      &:hover {
        opacity: 1;
        cursor: pointer;
      }
    }
  }
`;
