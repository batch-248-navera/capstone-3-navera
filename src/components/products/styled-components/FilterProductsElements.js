import styled from "styled-components";

export const Container = styled.div`
  width: 400px;
  height: 80vh;
  overflow: auto;
  background-color: white;
  transition: width 0.5s ease-out 0.7s;

  @media (max-width: 768px){
      display: none;
  }
`;

export const Filters = styled.div`
  display: flex;
  flex-direction: column;
  margin: 10px 0 0 20px;
  padding-bottom: 10px;
  border-bottom: 1px solid gray;
  

  & label {
    margin-left: 10px;
  }

  & label:hover {
    cursor: pointer;
  }
`;

export const Filter = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding-right: 10px;

  & :hover {
    cursor: pointer;
  }
`;
