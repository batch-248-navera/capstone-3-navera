import styled from "styled-components";


export const Container = styled.div`
    display: flex;
    justify-content: space-between;
    background-color: white;
    padding: 10px 20px;
    

    & .left{

        display: flex;
        gap: 10px;
    }
    & .right{

        display: flex;
        gap: 10px;

         & div{
            display: flex;
            align-items: center;
            gap: 4px;
            & :hover{
              cursor: pointer;
    }
        }
    }

   

    
`

export const Search = styled.div`
    display:flex;
    align-items: center;
    border: 1px solid grey;
    & input{
       
    }

    & span{
       
       & :hover{
           cursor: pointer;
    }

    @media (max-width:544px){
        width: 30px;
    }
        
    }


`
