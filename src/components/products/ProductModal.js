import { Container, Wrapper } from '../admin/styles/ManageProductStyles.js'

const ProductModal=(props)=> {
    
  return (
    <Container>
        <Wrapper>
            <div>
                <span onClick={props.onClose}>X</span>
                <div>
                    <img src={props.image} alt='product'/>
                </div>
                <div>
                    <h4>{props.name}</h4>
                    <span>{props.description}</span>
                    <span>{props.price}</span>
                    <button>Add to Cart</button>
                    <button>Buy</button>
                </div>
            </div>
        </Wrapper>
    </Container>
  )
}

export default ProductModal