import { useState } from "react";
import {
  Container,
  Filters,
  Filter,
} from "./styled-components/FilterProductsElements";
import { FaAngleUp, FaAngleDown } from "react-icons/fa";

const FilterProducts = () => {
  const [openBrands, setOpenBrands] = useState(true);
  const [openGender, setOpenGender] = useState(true);
  const [openAthletes, setOpenAthletes] = useState(true);
  const [openSale, setOpenSale] = useState(true);
  const [openPrice, setOpenPrice] = useState(true);

  const brandsFilterHandler = () => {
    setOpenBrands(prevOpenBrands=> !prevOpenBrands)
  };
  const genderFilterHandler = () => {
    setOpenGender(prevOpenGender=> !prevOpenGender)
  };
  const athletesFilterHandler = () => {
    setOpenAthletes(prevOpenAthletes=> !prevOpenAthletes)
  };

  const priceFilterHandler = () => {
    setOpenPrice(prevOpenPrice=> !prevOpenPrice)
  };
  const saleFilterHandler = () => {
    setOpenSale(prevOpenSale=> !prevOpenSale)
  };

  return (
    <Container>
      <Filters className="filter">
        <Filter onClick={brandsFilterHandler}>
          <h5>Brands</h5>
          <span>{openBrands ? <FaAngleUp /> : <FaAngleDown />}</span>
        </Filter>

        {openBrands && (
          <>
            <label>
              <input type="checkbox" />
              Nike
            </label>
            <label>
              <input type="checkbox" />
              Adidas
            </label>
            <label>
              <input type="checkbox" />
              Under Armour
            </label>
            <label>
              <input type="checkbox" />
              Air Jordan
            </label>
            <label>
              <input type="checkbox" />
              Reebok
            </label>
            <label>
              <input type="checkbox" />
              Fila
            </label>
          </>
        )}
      </Filters>
      <Filters className="filter">
        <Filter onClick={genderFilterHandler}>
          <h5>Gender</h5>
          <span>{openGender ? <FaAngleUp /> : <FaAngleDown />}</span>
        </Filter>
        {openGender && (
          <>
            <label>
              <input type="checkbox" />
              Kids
            </label>
            <label>
              <input type="checkbox" />
              Men
            </label>
            <label>
              <input type="checkbox" />
              Women
            </label>
          </>
        )}
      </Filters>
      <Filters className="filter">
        <Filter onClick={athletesFilterHandler}>
          <h5>Athletes</h5>
          <span>{openAthletes ? <FaAngleUp /> : <FaAngleDown />}</span>
        </Filter>
        {openAthletes && (
          <>
            <label>
              <input type="checkbox" />
              Ste
            </label>
            <label>
              <input type="checkbox" />
              Adidas
            </label>
            <label>
              <input type="checkbox" />
              Under Armour
            </label>
            <label>
              <input type="checkbox" />
              Jordan Air
            </label>
            <label>
              <input type="checkbox" />
              Reebok
            </label>
            <label>
              <input type="checkbox" />
              Fila
            </label>
          </>
        )}
      </Filters>

      <Filters>
        <Filter onClick={priceFilterHandler}>
          <h5>Price</h5>
          <span>{openPrice ? <FaAngleUp /> : <FaAngleDown />}</span>
        </Filter>
        {openPrice && (
          <>
            <label>
              <input type="checkbox" />
              ₱1-₱2000
            </label>
            <label>
              <input type="checkbox" />
              ₱2001-₱4000
            </label>
            <label>
              <input type="checkbox" />
              ₱4001-₱8000
            </label>
            <label>
              <input type="checkbox" />
              ₱8001 above
            </label>
          </>
        )}
      </Filters>
      <Filters>
        <Filter onClick={saleFilterHandler}>
          <h5>Sale</h5>
          <span>{openSale ? <FaAngleUp /> : <FaAngleDown />}</span>
        </Filter>
        {openSale && (
          <>
            <label>
              <input type="checkbox" />
              10%
            </label>
            <label>
              <input type="checkbox" />
              20%
            </label>
            <label>
              <input type="checkbox" />
              30%
            </label>
            <label>
              <input type="checkbox" />
              50%
            </label>
          </>
        )}
      </Filters>
    </Container>
  );
};

export default FilterProducts;
