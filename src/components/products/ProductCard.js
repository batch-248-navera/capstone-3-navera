import {useState, useContext} from 'react';
import AuthContext from '../../store/AuthContext';
import { createPortal } from 'react-dom';
import {Rating} from "flowbite-react"
import Cart from '../../pages/Cart'

import ViewProduct from '../home/ViewProduct';

const ProductCard = (props)=> {
  const {user} = useContext(AuthContext)
  
  const [product, setProduct] = useState("");
  const [viewModal, setViewModal] = useState(false);
 


  const addtoCart = (productId) =>{
		console.log(productId)
		fetch(`https://capstone-2-navera.onrender.com/products/${productId}/addToCart`, {
			method: "POST",
			headers: {
			  "Content-Type": "application/json",
			  Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				productId: productId,
				quantity: 1
			}),
		  })
			.then((res) => res.json())
			.then((data) => {
				
			  if (data) {
				
				alert("Product added to cart successfully!");
				
			  } else {
				alert("Failed!");
			  }
			});

	}



  return (
      
<div key={props.productId} class="w-full h-[400px] max-w-md bg-white border border-gray-200 shadow rounded-lg">
    <div>
        <img class="object-cover p-2 max-w-full h-[200px]"  src={props.image} alt="product" />
    </div>
    <div class="px-2 pb-5 text-center">
        <div className="pb-2">
            <h5 class="text-xl font-semibold tracking-tight text-gray-900 ">{props.name}</h5>
            <p class="text-md text-gray-400 ">{props.description}</p>
        </div>

        
       <Rating >
          <Rating.Star />
          <Rating.Star />
          <Rating.Star />
          <Rating.Star />
          <Rating.Star filled={false} />
        </Rating>
        <div class="flex items-center justify-between pb-1">
            <span class="text-md text-gray-900">₱{props.price}</span>
        </div>
        <div className='flex justify-around  w-full'>
         { user.id!==null?
         <button onClick={()=>{
                addtoCart(props.productId);

          }} className=' w-auto  bg-red-500 text-white text-xs py-1 px-1'>
            Add to cart
          </button>
          :
          <button onClick={()=>{
                addtoCart(props.productId);

          }} disabled className=' w-auto  bg-red-500 text-white text-xs py-1 px-1'>
            Add to cart
          </button>
          }
          <button onClick={()=> {
								  setViewModal(true);
									setProduct(props.productId);
									}} className='w-auto  bg-blue-500 text-white text-xs py-1 px-1'>
            View
          </button>
          {viewModal && createPortal(
       			  <ViewProduct onClose={() => setViewModal(false)} productId={product} />,document.body)}
        </div>
        </div>
    </div>
    


  
  );
}

export default  ProductCard