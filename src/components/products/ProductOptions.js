import { useState} from "react";
import { Container, Search } from "./styled-components/ProductOptionsStyles";
import {FaSearch, FaTasks} from 'react-icons/fa'

const ProductOptions = ({filterOption, openFilter}) => {


  const [open, setOpen] = useState(openFilter);

 const filterOptionHandler = ()=>{

    setOpen(prevOpen=>!prevOpen);
    filterOption(open);
 }


  return (
    <Container>
      
        <div  className="left">
          <Search>
            <input type="search" placeholder="Search product..."></input>
            <span><FaSearch /></span>
          </Search>
          
        </div>

        <div className="right">
          <div onClick={filterOptionHandler} >
            <span> Hide Filters</span> 
            <FaTasks />
          </div>
          
          <select name="products" id="product">
            <option value="low-high">Price: Low-High</option>
            <option value="high-low">Price: High-Low</option>
            <option value="best-sellers">Best sellers</option>
            <option value="newest">Newest</option>
          </select>
        </div>
      
    </Container>
  );
};

export default ProductOptions;
