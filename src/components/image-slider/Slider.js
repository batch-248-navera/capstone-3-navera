import { useState, useEffect } from "react";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";


const Slider = ({slides}) => {
  const [currentSlide, setCurrentSlide] = useState(0);
  const slideLength = slides.length;


  const autoScroll = true;
  let slideInterval;
  let intervalTime = 10000;

  const nextSlide = () => {
    setCurrentSlide(currentSlide === slideLength - 1 ? 0 : currentSlide + 1);
    console.log("next");
  };

  const prevSlide = () => {
    setCurrentSlide(currentSlide === 0 ? slideLength - 1 : currentSlide - 1);
    console.log("prev");
  };

  function auto() {
    slideInterval = setInterval(nextSlide, intervalTime);
  }

  useEffect(() => {
    setCurrentSlide(0);
  },[]);

  useEffect(() => {
    if (autoScroll) {
      auto();
    }
    return () => clearInterval(slideInterval);
  }, [currentSlide]);

  return (
    <div className="m-4">
      <div className="flex justify-center items-center border border-red-700">
      <span className="absolute top-50 left-20" onClick={prevSlide} ><FaAngleLeft/></span>
      <span className="absolute top-50 right-20" onClick={nextSlide} ><FaAngleRight/></span>
      <button className="absolute top-50 right-20 border-5 border-black">Shop Now</button>
      {slides.map((slide, index) => {
        return (
          <div className=" duration-700 ease-in-out" key={index} >
            {index === currentSlide && (
              
               <img className="block w-full h-auto object-cover" src={slide.image} alt="slide" />
                 
            )}
          </div>
        );
      })}
      </div>
    </div>
  );
};

export default Slider;