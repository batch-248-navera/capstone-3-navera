
import {useState, useEffect} from 'react';
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import ProductCard from "../products/ProductCard";
import { productData, responsive } from "./data";


const ProductSlider = () => {


  const [activeProducts, setActiveProducts] = useState([]);


  const fetchActiveProducts = () =>{
		fetch(`https://capstone-2-navera.onrender.com/products/active`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			
			
    setActiveProducts(data.map((product) => (
    
        <ProductCard
          key={product._id}
          productId={product._id}
          name={product.name}
          image={product.image}
          price={product.price}
          description={product.description}
        />
      
    ))
    )
		
								
			
		})
	}

	
	

	useEffect(()=>{
		fetchActiveProducts();
	},[])

  return (
    
      <Carousel  responsive={responsive}>
        {activeProducts}
      </Carousel>
   
  );
}

export default ProductSlider;