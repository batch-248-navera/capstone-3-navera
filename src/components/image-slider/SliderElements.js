import styled from "styled-components";
import { FaAngleLeft, FaAngleRight } from "react-icons/fa";

export const Wrapper = styled.div`
  width: 100%;
  height: 75vh;
  overflow: hidden;
 
`;

export const Slide = styled.div`
  width: 100%;
  transition: all 0.5s ease;
`;

export const Img = styled.img`
   width: 100%;
   height: 100%;
   object-fit: cover;
    
`;
export const Next = styled(FaAngleRight)`
   position: absolute;
   top: 40%;
   right: 5%;
   color: #ee4d2d;
   font-size: 3rem;
   border-radius: 50%;
   background-color: white;
   opacity: 0;
   &:hover{
    opacity: 1;
    cursor: pointer;
   }


`;
export const Prev = styled(FaAngleLeft)`
  position: absolute;
   top: 40%;
   left: 5%;
   color: #ee4d2d;
   font-size: 3rem;
   border-radius: 50%;
   background-color: white;
   opacity: 0;
   &:hover{
    opacity: 1;
    cursor: pointer;
    
   }
`;

export const Button = styled.button`
  position: absolute;
  top: 60%;
  left: 15%;
  color: #ff7337;
  font-size: 1.5rem;
  width: 150px;
  border: 1px solid #ff7337;
  &:hover{
    background: linear-gradient(#ee4d2d,#ff7337);
    color: white;
  }

`;
