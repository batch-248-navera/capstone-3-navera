import {useState} from 'react';
import DisplayProducts from "../components/products/DisplayProducts";
import FilterProducts from "../components/products/FilterProducts";
import ProductOptions from "../components/products/ProductOptions";
import { Container, Wrapper } from "./styled-components/ProductsElements";

const Products = () => {

    const [openFilter, setOpenfilter] = useState(true);

    const filterOption = (filter) =>{
      setOpenfilter(filter);
    }

  return (
    <Container>
      <Wrapper>
        {openFilter&&<FilterProducts />}
        <div>
          <ProductOptions filterOption={filterOption} openFilter={openFilter}/>
          <DisplayProducts />
        </div>
      </Wrapper>
    </Container>
  );
};

export default Products;
