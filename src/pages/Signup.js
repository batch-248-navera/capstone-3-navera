import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import AuthContext from "../store/AuthContext";
import { Container } from "./styled-components/SignupStyles";
import { FaUserCircle } from "react-icons/fa";
import Banner from "../components/home/Banner";

const Signup = () => {
  const { user } = useContext(AuthContext);
  const navigate = useNavigate();

  const [username, setUsername] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [gender, setGender] = useState("");
  const [bday, setBday] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const [isActive, setIsActive] = useState(false);



  const  register = (e) => {
    e.preventDefault();

    const createCart =(userId)=>{
      fetch(`https://capstone-2-navera.onrender.com/cart/create`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          userId: userId,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(userId);
          if(data === true){
            navigate("/login");
          }
          else{
            return false;
          }
          
    })
  }


    fetch(`https://capstone-2-navera.onrender.com/users/checkEmail`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          alert("email exist");
        } else {
          fetch(`https://capstone-2-navera.onrender.com/users/register`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              username: username,
              firstName: firstName,
              lastName: lastName,
              email: email,
              gender: gender,
              bday: bday,
              mobileNo: mobileNo,
              password: password,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              
              

              if (data !== false) {
                setUsername("");
                setFirstName("");
                setLastName("");
                setEmail("");
                setMobileNo("");
                setGender("");
                setBday("");
                setPassword("");
                setConfirmPassword("");

                alert("Succesful!");
                createCart(data._id);
                
              } 
              else {
                alert("Try again");
              }
            });
        }
      });
  }

  useEffect(() => {
    if (
      username !== "" &&
      firstName !== "" &&
      lastName !== "" &&
      email !== "" &&
      gender !== "" &&
      bday !== "" &&
      mobileNo.length === 11 &&
      password !== "" &&
      confirmPassword !== "" &&
      password === confirmPassword
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [
    username,
    firstName,
    lastName,
    email,
    gender,
    bday,
    mobileNo,
    password,
    confirmPassword,
  ]);

  return (

  <div className="grid gap-6 md:grid-cols-2 px-6 items-center">
  <div className="mt-3 hidden md:block"><Banner  /> </div>
  <div className="mt-3 p-4 border border-grey-500 rounded-md">
    <form onSubmit={(e) => register(e)}>
      
      <h2 className="text-2xl text-bold mb-3 text-center">Create Account</h2>
      
      <div className="grid gap-6 mb-1 md:grid-cols-2">
          <div>
              <label for="first_name" className="block mb-1 text-sm font-medium text-gray-900">First name</label>
              <input 
              type="text" 
              className="bg-gray-50  border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="Juan" required
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              />
              
          </div>
          <div>
              <label for="last_name" className="block mb-1 text-sm font-medium text-gray-900">Last name</label>
              <input
               type="text"
               className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="Dela Cruz" required
               value={lastName}
               onChange={(e) => setLastName(e.target.value)}
               />
          </div>
          <div >
            <label for="username" className="block mb-1 text-sm font-medium text-gray-900">Username</label>
            <input 
            type="text" 
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="Username" required
            value={username}
            onChange={(e) => setUsername(e.target.value)}/>
          </div> 

          <div >
            <label for="email" className="block mb-1 text-sm font-medium text-gray-900">Email address</label>
            <input 
            type="text" 
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="user@mail.com" required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
              />
          </div> 

          <div>
              <label for="phone" className="block mb-1 text-sm font-medium text-gray-900">Mobile number</label>
              <input 
              type="tel" 
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="09-123-45-678" required
              value={mobileNo}
              onChange={(e) => setMobileNo(e.target.value)}
              />
          </div>
          
          <div>
              <label for="bday" className="block mb-1 text-sm font-medium text-gray-900">Birthday</label>
              <input 
              type="date"  
              className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="" required
              value={bday}
              onChange={(e) => setBday(e.target.value)}
              />
          </div> 
          
      </div>
    <label for="gender" className="block mb-1 text-sm font-medium text-gray-900">Gender</label>
    <div className="flex gap-6 my-2" onChange={(e) => setGender(e.target.value)}>
      
    <div className="flex items-center mr-4">
        <input  type="radio" value="Male" name="gender" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
        <label for="inline-radio" className="ml-2 text-sm font-medium text-gray-900 ">Male</label>
    </div>
    <div className="flex items-center mr-4">
        <input  type="radio" value="Female" name="gender" className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 focus:ring-2 dark:bg-gray-700 dark:border-gray-600"/>
        <label for="inline-2-radio" className="ml-2 text-sm font-medium text-gray-900 ">Female</label>
    </div>
  
    
</div>
     
      <div className="mb-2">
          <label for="password" className="block mb-1 text-sm font-medium text-gray-900">Password</label>
          <input 
          type="password" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="•••••••••" required
          value={password}
          onChange={(e) => setPassword(e.target.value)}/>
      </div> 
      <div className="mb-2">
          <label for="confirm_password" className="block mb-1 text-sm font-medium text-gray-900">Confirm password</label>
          <input 
          type="password"  
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="•••••••••" required
          value={confirmPassword}
          onChange={(e) => setConfirmPassword(e.target.value)}/>
      </div> 
      <div className="flex items-start mb-2">
          <div className="flex items-center h-5">
          <input  type="checkbox" value="" className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-blue-300" required/>
          </div>
          <label  className="ml-2 text-sm font-medium text-gray-900 ">I agree with the <a href="#" className="text-blue-600 hover:underline">terms and conditions</a>.</label>
      </div>
      {isActive?
            <button type="submit" className="text-white bg-black  focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center ">Submit</button>
            :
            <button type="submit" className="text-white bg-black  focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center " disabled>Submit</button>
          }

  </form>
</div>
</div>
  );
};

export default Signup;
