import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    background-color: #f5f5f5;
    padding-top:100px;


    
    & .right{
        position: relative;
        display: flex;
        align-items: center;
        width: 50%;

        @media (max-width:768px){
            display: none;
        }
        
        
    }

    & .left {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        background-color: white;
        border-radius: 5px;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        width: 450px;
        height: 450px;
        
        & span{
            font-size: 5rem;
        }
        &  h4{
            font-size: 2rem;
        }
       

        & form{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            gap: 2rem;

            & input{
                width: 300px;
                border: 1px solid gray;
            }

            & ul{
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                gap: 50px;
                width: 300px;
                padding: 0;

                & li{
                    font-size:12px;
                }
            }

            & button{
            padding:4px;
            width: 300px;
            color: white;
            background-color: black;

        }
            
        }
    }
`