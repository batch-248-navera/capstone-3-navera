import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    background-color: #f5f5f5;
    padding-top:100px;


    
    & .left{
        position: relative;
        display: flex;
        align-items: center;
        width: 50%;

        @media (max-width:768px){
            display: none;
        }
        
        
    }

    & .right {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        background-color: white;
        border-radius: 5px;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
        width: 500px;
        height: 85vh;
        
        & span{
            font-size: 4rem;
        }
        &  h4{
            font-size: 2rem;
            margin-bottom: 10px;
        }
       

        & form{
            display: flex;
            flex-direction: column;
            justify-content: flex-start;
            align-items: flex-end;
            gap: 1rem;

           

            & ul{
                display: flex;
                flex-direction: row;
                justify-content: space-between;
                gap: 50px;
                width: 300px;
                padding: 0;

                & li{
                    font-size:12px;
                }
            }

            div{
                display: flex;
                gap: 4px;

                input{
                width: 300px;
                border: 1px solid grey;

            }
            }
            & .name{

                div{

                    width: 300px;
            
                    

                    & input:first-child{
                   
                       width: 200px;
                       
                }
            
                    & input:last-child{
                        width: 100px;
                    }
                }
               
            }
           
            & .gender{
                display: flex;
                justify-content: space-between;
                
                div{
                    width: 300px;
                }
            }

           
            }
            & .password{

                & div{
                  width: 300px;  
                }
                
                
                & input{
                    width: 150px;
                }

            }

            & button{
            padding:4px;
            width: 200px;
            color: white;
            align-self: center;
            background-color: black;

        }

        div:last-child{
            align-self: center;

            span{
                font-size: 1em;

                &:hover{
                    cursor: pointer;
                }

            }
        }
            
        }
    
`;
