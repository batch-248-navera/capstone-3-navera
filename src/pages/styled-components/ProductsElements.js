import styled from "styled-components";

export const Container =styled.div`
    display: flex;
    flex-direction: column;
    gap: 20px;
    padding: 100px 0 50px 0;
    background-color: #f5f5f5;

`
export const Wrapper = styled.div`
    display: flex;
    margin-top: 20px
`