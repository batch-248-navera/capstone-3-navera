import {useState, useEffect, useContext} from 'react';
import  AuthContext from '../store/AuthContext';
import Banner from "../components/home/Banner";
import { FaUserCircle } from "react-icons/fa";
import { Navigate, useNavigate } from 'react-router-dom';



const Login = () => {

  const { user, setUser } = useContext(AuthContext);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

 

  const auth = (e) =>{

		e.preventDefault();

		fetch('https://capstone-2-navera.onrender.com/users/login',{
			method:'POST',
			headers:{
				'Content-Type':'application/json'
			},
			body: JSON.stringify({
				email:email,
				password:password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token',data.access)
				retrieveUserDetails(data.access)
				console.log(data.isAdmin)
			
			  
				
			  alert("Login succesful!")
			  
			  


			}else{

				alert("Login failed!")

			}

			

		})

		const retrieveUserDetails = (token) =>{

			fetch('https://capstone-2-navera.onrender.com/users/details',{
				headers:{
					Authorization: `Bearer ${token}`
				}
			})
			.then(res=>res.json())
			.then(data=>{
				

				setUser({
					id: data._id,
					isAdmin: data.isAdmin,
					image: data.image,
					username: data.username,
					firstName: data.firstName,
					lastName: data.lastName,
					email: data.email,
					mobileNo: data.mobileNo,
					bday: data.bday,
					gender: data.gender,
					password: data.password

				})
			
			
	  

			})
		}


		
		setEmail("");
		setPassword("");


	}


	useEffect(()=>{

		if(email !== "" && password !== ""){
			setIsActive(true)
		}else{
			setIsActive(false)
		}

	},[email, password])





  return (
	
	(user.id!==null)?

	
	(user.isAdmin===true)?
			  <Navigate to="/admin/products"/>
				:
			 <Navigate to="/"/>
	
	:
	
    <div className="grid gap-6 md:grid-cols-2 px-6 items-center">
      <div className="mt-3 hidden md:block">
        <Banner />
      </div>  
	  <div className="mt-3 p-4 border border-grey-500 rounded-md">
	<div className='flex items-center flex-col mb-5'>
		<span className='text-center text-6xl'>
          <FaUserCircle />
        </span>
	<h2 className='text-center text-2xl text-bold'>Login</h2>
	
	</div>
	  
    <form onSubmit={(e) => auth(e)}>
		<div className="mb-4">
            <label for="email" className="block mb-1 text-sm font-medium text-gray-900">Email address</label>
            <input 
            type="text" 
            className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="user@mail.com" required
            value={email}
            onChange={(e) => setEmail(e.target.value)}
              />
          </div> 

  
    

     
      <div className="mb-4">
          <label for="password" className="block mb-1 text-sm font-medium text-gray-900">Password</label>
          <input 
          type="password" 
          className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-1 " placeholder="•••••••••" required
          value={password}
          onChange={(e) => setPassword(e.target.value)}/>
      </div> 
      
      
      {isActive?
            <button type="submit" className="text-white bg-black  focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center ">Login</button>
            :
            <button type="submit" className="text-white bg-black  focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center " disabled>Login</button>
          }

</form>
</div>
      </div>
   

  );
};

export default Login;
