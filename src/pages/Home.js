import { useContext} from 'react';
import  AuthContext from '../store/AuthContext';
import { Navigate } from 'react-router-dom';
import Banner from '../components/home/Banner';
import FeaturedCollections from '../components/home/FeaturedCollections';
import Category from '../components/home/Category';
import BestSeller from '../components/home/BestSeller';
import NewArrivals from '../components/home/NewArrivals';


const Home = () => {
    const { user, setUser } = useContext(AuthContext);

    return (
        
        (user.isAdmin)?
		<Navigate to="/admin/dashboard"/>
		:

        <div className='grid grid-col gap-6 bg-[#f5f5f5]'>
            <Banner/>
            <h1 className='text-2xl text-bold text-center'>Featured Collections</h1>
            <FeaturedCollections/>
            <h1 className='text-2xl text-bold text-center'>🔥Hot Products</h1>
            <NewArrivals/>  
            <h1 className='text-2xl text-bold text-center'>Category</h1>
            <Category/>  
            <h1 className='text-2xl text-bold text-center'>Latest Products</h1>
            <BestSeller/>
        </div> 
        
    );

}


export default Home;