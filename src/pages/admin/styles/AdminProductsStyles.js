import styled from "styled-components";
import {Link} from "react-router-dom";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: 100%;
    background-color: #f5f5f5;

    div{
        margin: 10px 4px 20px 4px;

    }

    `
export const Button = styled(Link)`

`

export const Table = styled.table`

    margin: 0 4px;

    thead{
        background-color: black;
        color: white;
    }
    `
export const TableRow = styled.tr`


     td{
        border: 1px solid gray;
     }
    
    td:last-child{
            display: flex;
            justify-content: center;
            align-items: center;
            gap: 5px;

            button:first-child{
                color: red;
    
            }
            button:last-child{
                color: yellow;
    
            }

        }
    
    `
