import styled from "styled-components";

export const Container = styled.div`
    display: flex;
    flex-direction: column;
    width: calc(100%-200px);
    height: 100%;

    & h1{

        color: #ee4d2d;
        font-size: 20px;
        margin-top: 20px;
        padding-left: 100px;
    }
    

`

export const Cards = styled.div`

    display: flex;
    justify-content: space-evenly;
    flex-wrap: wrap;
    width: 100%;
    margin-top: 50px;
    `
export const Card = styled.div`

    display: flex;
    
    width: 250px;
    height: 100px;
    border-radius: 5px;
    background-color: #0093E9;
    
    &.total-order{
        background-image: linear-gradient( 135deg, #81FBB8 10%, #28C76F 100%);

    }
    &.total-sales{
        background-image: linear-gradient( 135deg, #ABDCFF 10%, #0396FF 100%);

    }
    &.total-users{
        background-image: linear-gradient( 135deg, #CE9FFC 10%, #7367F0 100%);

    }
    &.total-products{
        background-image: linear-gradient( 135deg, #FDEB71 10%, #F8D800 100%);

    }
   
`

export const Charts = styled.div`
    display: flex;
    justify-content: space-evenly;
    align-items: center;
    margin-top: 20px;

    & div:nth-child(1){
        width: 700px;
    }

    & div:nth-child(2){
        width: 300px;
    }
`
export const Orders = styled.div`
    display: flex;

    & div{
        width: 400px;
    }
`

