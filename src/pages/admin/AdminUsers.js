import { useContext, useState, useEffect } from "react";
import {Table, Container} from "./styles/AdminProductsStyles";
import {Navigate, Link, useNavigate} from "react-router-dom";
import { createPortal } from 'react-dom';
import AuthContext from "../../store/AuthContext";
import DeleteProduct from "../../components/admin/DeleteProduct";
import {FaRegEdit, FaTrashAlt} from 'react-icons/fa';
import ActivateProduct from "../../components/admin/ActivateProduct";

const AdminUsers = () =>{

	const {user} = useContext(AuthContext);

	const [deleteModal, setDeleteModal] = useState(false);
	const [activateModal, setActivateModal] = useState(false);
	const [isActive, setIsActive] = useState(false);
	
	const [productId, setProductId] = useState(false);
	const [allUsers, setAllUsers] = useState([]);

	const navigate = useNavigate();

	const deleteProductHandler = (id) =>{
		setDeleteModal(true);
		setProductId(id);
	}


	const fetchData = () =>{
		fetch(`https://capstone-2-navera.onrender.com/users/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
      console.log(data)
	

			setAllUsers(data.map(user => {
				return(
					
					<tr key={user._id}>
						
						<td>{user._id}</td>
						<td>{user.username}</td>
						<td>{user.firstName.concat(` ${user.lastName}`)}</td>
						<td>{user.email}</td>
						<td>{user.gender}</td>
						<td>{user.mobileNo}</td>
						<td>
							<button onClick={()=> deleteProductHandler(user._id)}><FaTrashAlt/></button>
							<button onClick={()=> navigate(`update/${user._id}`)}><FaRegEdit/></button>
							
							
						</td>
						
					</tr> 
				)
			}))

		})
	}

	
	

	useEffect(()=>{
		fetchData();
	})

	return(
		(user.isAdmin)
		?
        <Container>
							
				{deleteModal && createPortal(
       			  <DeleteProduct onClose={() => setDeleteModal(false)} productId={productId}/>,document.body)}
				{activateModal && createPortal(
       			  <ActivateProduct onClose={() => setActivateModal(false)} productId={productId} isActive={isActive}/>,document.body)}
		    <div>
				<h2>Users</h2>
					<button onClick={()=> navigate("add")}>Add user</button>
			</div>
			<Table>
		     <thead>
		       <tr>
		         <th>User ID</th>
		         <th>Username</th>
		         <th>Name</th>
		         <th>Email</th>
             	 <th>Gender</th>
		         <th>Mobile No.</th>
		         <th>Action</th>
		       </tr>
		     </thead>
		     <tbody>
		       { allUsers }
			  
		     </tbody>
		   </Table>
		</Container>
		:
		<Link to="/" />
	)
}


export default AdminUsers;