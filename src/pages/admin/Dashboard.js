import { Container, Cards, Card,  Charts } from "./styles/DashboardElements";
import {TbCurrencyPeso} from 'react-icons/tb'
import {MdInventory2} from 'react-icons/md'
import {AiOutlineShoppingCart, AiOutlineUser} from 'react-icons/ai'
import { Bar, Pie } from "react-chartjs-2";
import { Chart as ChartJS } from "chart.js/auto";
import { useState } from 'react';

const Dashboard = () =>{
     const UserData = [
        {
          id: 1,
          year: 2016,
          userGain: 80000,
          userLost: 823,
        },
        {
          id: 2,
          year: 2017,
          userGain: 45677,
          userLost: 345,
        },
        {
          id: 3,
          year: 2018,
          userGain: 78888,
          userLost: 555,
        },
        {
          id: 4,
          year: 2019,
          userGain: 90000,
          userLost: 4555,
        },
        {
          id: 5,
          year: 2020,
          userGain: 4300,
          userLost: 234,
        },
      ];

      const [userData, setUserData] = useState({
    labels: UserData.map((data) => data.year),
    datasets: [
      {
        label: "Users Gained",
        data: UserData.map((data) => data.userGain),
        backgroundColor: [
          "rgba(75,192,192,1)",
          "#ecf0f1",
          "#50AF95",
          "#f3ba2f",
          "#2a71d0",
        ],
        borderColor: "black",
        borderWidth: 2,
      },
    ],
  });

    return (
        <Container>
            <h1>Dashboard</h1>

            <Cards>
                <Card className="total-order">
                    <span>Total Orders</span>
                    <span>213</span>
                    <span><AiOutlineShoppingCart/></span>
                </Card>               
                <Card className="total-sales">
                    <span>Total Sales</span>
                    <span>324</span>
                    <span><TbCurrencyPeso/></span>
                </Card>               
                <Card className="total-users">
                    <span>Total Users</span>
                    <span>234</span>
                    <span><AiOutlineUser/></span>
                </Card>               
                <Card className="total-products">
                    <span>Total Products</span>
                    <span>42424</span>
                    <span><MdInventory2/></span>
                </Card>               
                              
            </Cards>
            <Charts>

             <div>
                <Bar data={userData} />
             </div>
             <div>
                <Pie data={userData} />
             </div>
              
              
            </Charts>
        </Container>
    );
}

export default Dashboard;