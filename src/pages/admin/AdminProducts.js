import { useContext, useState, useEffect } from "react";
import {Table} from "flowbite-react";
import {Navigate, Link, useNavigate} from "react-router-dom";
import { createPortal } from 'react-dom';
import AuthContext from "../../store/AuthContext";
import AddProduct from "../../components/admin/AddProduct";
import EditProduct from "../../components/admin/EditProduct";
import DeleteProduct from "../../components/admin/DeleteProduct";
import ActivateProduct from "../../components/admin/ActivateProduct";
import {FaRegEdit, FaTrashAlt} from 'react-icons/fa';

const AdminProducts = () =>{

	const {user} = useContext(AuthContext);

	const [editModal, setEditModal] = useState(false);
	const [addModal, setAddModal] = useState(false);
	const [deleteModal, setDeleteModal] = useState(false);
	const [activateModal, setActivateModal] = useState(false);
	const [isActive, setIsActive] = useState(false);
	
	const [productId, setProductId] = useState(false);
	const [allProducts, setAllProducts] = useState([]);

	const navigate = useNavigate();

	

	useEffect(()=>{
			fetch(`https://capstone-2-navera.onrender.com/products/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			

			setAllProducts(data.map(product => {

				return(
					
					<Table.Row key={product._id} className="bg-white text-center text-black px-1">
					
					<Table.Cell className="whitespace-nowrap font-medium text-gray-900 ">
					{product._id}
					</Table.Cell>
					<Table.Cell className="whitespace-nowrap font-medium text-gray-900 ">
					{product.name}
					</Table.Cell>
				
					<Table.Cell>
					{product.description}
					</Table.Cell>
					<Table.Cell>
					{product.price}
					</Table.Cell>
					{product.isActive?
					<Table.Cell className="text-green-500">
					Active
					</Table.Cell>
					:
					<Table.Cell className= "text-red-500">
					Inactive
					</Table.Cell>

					}
					
					<Table.Cell className="flex gap-2 justify-center">
					<button onClick={()=> {
									setDeleteModal(true);
									setProductId(product._id);
								}} className="text-red-500"><FaTrashAlt/></button>
								<button onClick={()=> {
								    setEditModal(true);
									setProductId(product._id);
									}} className="text-blue-500" ><FaRegEdit/></button>
								{product.isActive?
								<button onClick={()=>{
									setActivateModal(true);
									setProductId(product._id);
									setIsActive(product.isActive);
								}} className="bg-red-500 rounded-lg w-20 text-white px-1">
									Deactivate
								</button>
								:
								<button onClick={()=>{
									setActivateModal(true);
									setProductId(product._id);
									setIsActive(product.isActive);
								}} className="bg-green-500 rounded-lg w-20 text-white px-1">
									Activate
								</button>
									}	

					</Table.Cell>
				</Table.Row>
				);
			})						
		
		)
		})
	})
		 
		

	return( 

		(user.isAdmin)
		?

		<div className="py-6 pt-2 bg-[#f5f5f5]">
			{deleteModal && createPortal(
       			  <DeleteProduct onClose={() => setDeleteModal(false)} productId={productId}/>,document.body)}
				{activateModal && createPortal(
       			  <ActivateProduct onClose={() => setActivateModal(false)} productId={productId} isActive={isActive}/>,document.body)}
		    	{editModal && createPortal(
       			  <EditProduct onClose={() => setEditModal(false)} productId={productId} />,document.body)}
		    	{addModal && createPortal(
       			  <AddProduct onClose={() => setAddModal(false)}  />,document.body)}
			<div className="flex flex-col mx-4 mb-2">
				<h2 className="text-2xl text-bold ">Products</h2>
					<button onClick={()=> {
						setAddModal(true);
									}}
						className="bg-blue-900 text-white px-2 rounded-lg self-end">+ Add product</button>
			</div>
		<Table hoverable={true} className="pt-20">
  <Table.Head className="text-center bg-grey-500">
    <Table.HeadCell>
      Product ID
    </Table.HeadCell>
    <Table.HeadCell>
      Product
    </Table.HeadCell>
    <Table.HeadCell>
	Description
    </Table.HeadCell>
    <Table.HeadCell>
     Price
    </Table.HeadCell>
    <Table.HeadCell>
     Status
    </Table.HeadCell>
    <Table.HeadCell>
      Action
    </Table.HeadCell>
  </Table.Head>
  <Table.Body className="divide-y">
    {allProducts}
			
  </Table.Body>
</Table>
</div>
		:
		<Link to="/" />
	)
}


export default AdminProducts;