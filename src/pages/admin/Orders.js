import { useContext, useState, useEffect } from "react";
import {Table, Container} from "./styles/AdminProductsStyles";
import {Navigate, Link, useNavigate} from "react-router-dom";
import { createPortal } from 'react-dom';
import AuthContext from "../../store/AuthContext";
import DeleteProduct from "../../components/admin/DeleteProduct";
import {FaRegEdit, FaTrashAlt} from 'react-icons/fa';
import ActivateProduct from "../../components/admin/ActivateProduct";

const Orders = () =>{

	const {user} = useContext(AuthContext);

	const [deleteModal, setDeleteModal] = useState(false);
	const [activateModal, setActivateModal] = useState(false);
	const [isActive, setIsActive] = useState(false);
	
	const [productId, setProductId] = useState("");
	const [orderNo, setorderNo] = useState(1);
	const [productName, setProductName] = useState("");
	const [productImage, setProductImage] = useState("");
	const [productPrice, setProductPrice] = useState("");
	const [allOrders, setAllOrders] = useState([]);

	const navigate = useNavigate();

	

    useEffect(() => {
  
        fetch(`https://capstone-2-navera.onrender.com/products/${productId}`)
        .then((res) => res.json())
        .then((data) => {
          console.log(data.name, data.price);
            
          setProductImage(data.image);
          setProductName(data.name);
          setProductPrice(data.price);
          
        });
},[productId])


	const fetchOrders = () =>{
		fetch(`https://capstone-2-navera.onrender.com/carts/all`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
      console.log(data)
	


			setAllOrders(data.map((order) => {
				return(
					order.products.map((product)=>{
                        setorderNo((prevOrderNo)=>{prevOrderNo += 1})
                        setProductId(product.productId);
                        return(

                        <tr key={orderNo}>
						
                            <td>{orderNo}</td>
                            <td>{order._id}</td>
                            <td>{productName}</td>
                            <td>{productPrice}</td>
                            <td>{order.purchasedOn}</td>
                            
                            <td>
                                <button ><FaTrashAlt/></button>
                           
                            </td>
						
					</tr> 


                   )
                })
					
				)
			}))

		})
	}

	
	

	useEffect(()=>{
		fetchOrders();
	})

	return(
		(user.isAdmin)
		?
        <Container>
							
				{deleteModal && createPortal(
       			  <DeleteProduct onClose={() => setDeleteModal(false)} productId={productId}/>,document.body)}
				{activateModal && createPortal(
       			  <ActivateProduct onClose={() => setActivateModal(false)} productId={productId} isActive={isActive}/>,document.body)}
		    <div>
				<h2>Users</h2>
					<button onClick={()=> navigate("add")}>Add order</button>
			</div>
			<Table>
		     <thead>
		       <tr>
		         <th>#</th>
		         <th>Order Id</th>
		         <th>Product Name</th>
		         {/* <th>Address</th> */}
		         <th>Date</th>
             	 <th>Price</th>
		         {/* <th>Status</th>
		         <th>Action</th> */}
		       </tr>
		     </thead>
		     <tbody>
		       { allOrders }
			  
		     </tbody>
		   </Table>
		</Container>
		:
		<Link to="/" />
	)
}


export default Orders;