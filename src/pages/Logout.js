import { useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import AuthContext from '../store/AuthContext';

const Logout = () =>{


	const { unsetUser, setUser } = useContext(AuthContext);

	
	unsetUser();

	useEffect(()=>{

		setUser({id:null})

	},[])

	return(

		<Navigate to="/"/>

	)

}

export default Logout;