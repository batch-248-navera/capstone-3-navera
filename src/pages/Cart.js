import { useContext, useState, useEffect } from "react";
import {Navigate, Link, useNavigate} from "react-router-dom";
import { createPortal } from 'react-dom';
import AuthContext from "../store/AuthContext";
import {FaMinus,FaPlus, FaTrashAlt, FaShoppingCart} from 'react-icons/fa';
import { Table } from 'flowbite-react'

const Cart = () =>{

	const {user} = useContext(AuthContext);

	
	
	const [cartData, setCartData] = useState([]);
	const [cartItems, setCartItems] = useState([]);
	const [qty, setQty] = useState(0)
	const [total, setTotal] = useState(0)
	const navigate = useNavigate();

	
	
	useEffect(()=>{

		fetch(`https://capstone-2-navera.onrender.com/cart/`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
					}
				})
			.then(res=> res.json())
			.then(data=>{
				setCartData(data)
				
			console.log(cartData)
			})
			.catch(error=>console.error(error));

			fetch(`https://capstone-2-navera.onrender.com/cart/userCart`,{
				headers:{
					"Authorization": `Bearer ${localStorage.getItem("token")}`
						}
					})
				.then(res=> res.json())
				.then(data=>{
					setTotal(data.totalPrice)
					
				
				})
				.catch(error=>console.error(error));

	},[]);

		

		 const removeItem = async(productId) =>{
			 fetch(`https://capstone-2-navera.onrender.com/cart/${productId}/deleteCartItem`, {
				method: "DELETE",
				headers: {
				  "Content-Type": "application/json",
				  Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				
			  })
			.then(res => res.json()) 
			.then(data=>console.log(data))
			.catch(error=>console.error(error))
			
			

			
		 }
		
		
		 const addQuantity = (index) =>{

			const cartProducts = [...cartData];
			cartProducts[index].quantity += 1;
			cartProducts[index].subTotal = cartProducts[index].quantity * cartProducts[index].price ;
			
			setQty(cartProducts)
			updateQuantity(cartProducts[index].productId,cartProducts[index].quantity)
		 }

		 const subQuantity = (index) =>{

			if(cartData[index].quantity>1) {
			const  cartProducts=[...cartData];
			cartProducts[index].quantity -=1;
			cartProducts[index].subTotal = cartProducts[index].quantity * cartProducts[index].price ;
			setQty(cartProducts);
			updateQuantity(cartProducts[index].productId,cartProducts[index].quantity)
			}
		 }

		 const updateQuantity = (productId,quantity) =>{

		setTotal(cartData.map(({subTotal})=> subTotal).reduce((a,c)=>a+c)) 

		 fetch(`https://capstone-2-navera.onrender.com/cart/${productId}/updateQuantity`, {
				method: "PATCH",
				headers: {
				  "Content-Type": "application/json",
				  Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				body: JSON.stringify({
				  quantity: quantity
				  
				}),
			  })
			  .then(res=> res.json())
			  .then(data=>console.log(data))
			  .catch(error=>console.error(error));
				
			  
			};
			
		 
		


		 const cart = cartData.map(({productId,name, quantity, price, subTotal},index)=>{
			
			
			return(
				<Table.Row key={productId} className="bg-white text-center text-black">
					
					<Table.Cell className="whitespace-nowrap font-medium text-gray-900 ">
						{name}
					</Table.Cell>
					<Table.Cell>
						₱{price}
					</Table.Cell>
					<Table.Cell>
						<div  className="flex justify-center">

							<button  onClick={()=>{subQuantity(index)}} className="text-xl  font-bold bg-black  text-white"><FaMinus/></button>
							<input className=" text-center text-black w-10  h-6 " type="number" value={quantity} />
							<button onClick={()=>{addQuantity(index)}} className="text-xl font-bold bg-black  text-white"><FaPlus/></button>
						</div>
					</Table.Cell>
					<Table.Cell>
						₱{subTotal}
					</Table.Cell>
					<Table.Cell>
						<span onClick={()=>{removeItem(productId)}} className="flex justify-center text-red-400 text-xl hover:cursor-pointer"><FaTrashAlt/></span>
					</Table.Cell>
				</Table.Row>
				);
 
		})
		 

	return(
	cartData.length>0?
		<div className="py-4 pt-4 bg-[#f5f5f5]">
		<Table hoverable={true} className="pt-20">
  <Table.Head className="text-center bg-grey-500">
    <Table.HeadCell>
      Product
    </Table.HeadCell>
    <Table.HeadCell>
      Unit Price
    </Table.HeadCell>
    <Table.HeadCell>
      Quantity
    </Table.HeadCell>
    <Table.HeadCell>
     Subtotal
    </Table.HeadCell>
    <Table.HeadCell>
      Action
    </Table.HeadCell>
  </Table.Head>
  <Table.Body className="divide-y">
    {cart}
	<Table.Row  className="bg-white text-center">
					
					
					<Table.Cell/>
					<Table.Cell/>
					<Table.Cell/>
						
					<Table.Cell>
						<span className="font-bold text-xl text-blue-500 ">Total: ₱{total}</span>
					</Table.Cell>
					<Table.Cell  className="flex text-black justify-center">
						<Link  to='/checkout' className="px-4 py-2 bg-black text-white">Checkout</Link>
					</Table.Cell>
	</Table.Row>
				
  </Table.Body>
</Table>
</div>
:

<div className="flex flex-col justify-center h-96 w-full items-center">
	<span className=" text-2xl mb-1"><FaShoppingCart/></span>
	<span className="  text-red-500">Your cart is empty.</span>
</div>

	)
}


export default Cart;