import { useState, useEffect, useContext } from "react";
import { useNavigate } from "react-router-dom";
import { createPortal } from "react-dom";
import AuthContext from "../store/AuthContext";
import { Container } from "./styled-components/SignupStyles";
import { FaUserCircle } from "react-icons/fa";
import Banner from "../components/home/Banner";
import ProfileModal from "../components/home/ProfileModal";

const UserProfile = () => {
  const { user } = useContext(AuthContext);

  const navigate = useNavigate();

  const [username, setUsername] = useState(user.username);
  const [firstName, setFirstName] = useState(user.firstName);
  const [lastName, setLastName] = useState(user.lastName);
  const [email, setEmail] = useState(user.email);
  const [gender, setGender] = useState(user.gender);
  const [bday, setBday] = useState(user.bday);
  const [mobileNo, setMobileNo] = useState(user.mobileNo);
  const [password, setPassword] = useState(user.password);
  const [confirmPassword, setConfirmPassword] = useState("");

  const [showModal, setShowModal] = useState(false);
  const [update, setUpdate] = useState("");

  const SaveUpdate = () => {
    fetch(`https://capstone-2-navera.onrender.com/cart/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({}),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data === true) {
          navigate("/login");
        } else {
          return false;
        }
      });
  };

  return (
    <div className="grid gap-6 px-6 items-center">
      <div className="mt-3  border border-grey-500 rounded-md">
        <div className="shadow-md px-4 py-2">
          <h4>My Profile</h4>
          <span>Manage and protect your account</span>
        </div>
        <div className="flex flex-col justify-center mt-4">
          <div className="grid md:grid-cols-2">
            <div className="flex justify-center flex-col items-center gap-8">
              <div className="border-2 border-dashed border-grey-200 rounded-full h-[200px] w-[200px]">
                <img className="w-full" src="" alt="Profile" />
              </div>

              <input type="file" />
            </div>

            <div className="place-items-center">
              <div className="flex gap-4 items-center mb-3">
                <span className=" text-sm font-medium text-gray-900">
                  Username:
                </span>
                <span>{username}</span>
                <button
                  onClick={() => {
                    setShowModal(true);
                    setUpdate("username");
                  }}
                  className="text-blue-500 underline decoration-blue-500"
                >
                  Update
                </button>
                {showModal &&
                  createPortal(
                    <ProfileModal
                      onClose={() => setShowModal(false)}
                      update={update}
                      setUsername={setUsername}
                    />,
                    document.body
                  )}
              </div>

              <div className="flex gap-4 items-center mb-3">
                <span className=" text-sm font-medium text-gray-900">
                  Name:
                </span>
                <span>{`${firstName} ${lastName}`}</span>
                <button
                  onClick={() => {
                    setShowModal(true);
                    setUpdate("name");
                  }}
                  className="text-blue-500 underline decoration-blue-500"
                >
                  Update
                </button>
                {showModal &&
                  createPortal(
                    <ProfileModal
                      onClose={() => setShowModal(false)}
                      update={update}
                      setFirstName={setFirstName}
                      setLastName={setLastName}
                    />,
                    document.body
                  )}
              </div>

              <div className="flex gap-4 items-center mb-3">
                <span className=" text-sm font-medium text-gray-900">
                  Email address:
                </span>
                <span>{email}</span>
                <button
                  onClick={() => {
                    setShowModal(true);
                    setUpdate("email");
                  }}
                  className="text-blue-500 underline decoration-blue-500"
                >
                  Update
                </button>
                {showModal &&
                  createPortal(
                    <ProfileModal
                      onClose={() => setShowModal(false)}
                      update={update}
                      setEmail={setEmail}
                    />,
                    document.body
                  )}
              </div>

              <div className="flex gap-4 items-center mb-3">
                <span className=" text-sm font-medium text-gray-900">
                  Mobile number:
                </span>
                <span>{mobileNo}</span>
                <button
                  onClick={() => {
                    setShowModal(true);
                    setUpdate("mobileNo");
                  }}
                  className="text-blue-500 underline decoration-blue-500"
                >
                  Update
                </button>
                {showModal &&
                  createPortal(
                    <ProfileModal
                      onClose={() => setShowModal(false)}
                      update={update}
                      setMobileNo={setMobileNo}
                    />,
                    document.body
                  )}
              </div>

              <div className="flex gap-4 items-center mb-3">
                <span className=" text-sm font-medium text-gray-900">
                  Birthday:
                </span>
                <span>{bday}</span>
                <button
                  onClick={() => {
                    setShowModal(true);
                    setUpdate("bday");
                  }}
                  className="text-blue-500 underline decoration-blue-500"
                >
                  Update
                </button>
                {showModal &&
                  createPortal(
                    <ProfileModal
                      onClose={() => setShowModal(false)}
                      update={update}
                      setBday={setBday}
                    />,
                    document.body
                  )}
              </div>
              <div className="flex gap-4 items-center mb-3">
                <span className=" text-sm font-medium text-gray-900">
                  Gender:
                </span>
                <span>{gender}</span>
                <button
                  onClick={() => {
                    setShowModal(true);
                    setUpdate("gender");
                  }}
                  className="text-blue-500 underline decoration-blue-500"
                >
                  Update
                </button>
                {showModal &&
                  createPortal(
                    <ProfileModal
                      onClose={() => setShowModal(false)}
                      update={update}
                      setGender={setGender}
                    />,
                    document.body
                  )}
              </div>

              <button  onClick={() => {
                    setShowModal(true);
                    setUpdate("password");
                  }} className="text-blue-500 underline decoration-blue-500">
                Change password
              </button>
              {showModal &&
                  createPortal(
                    <ProfileModal
                      onClose={() => setShowModal(false)}
                      update={update}
                      setPassword={setPassword}
                    />,
                    document.body
                  )}
            </div>
          </div>

          <button
            type="submit"
            className="text-white bg-black  focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full md:w-30 sm:w-auto px-5 py-2.5 text-center self-end m-8"
          >
            Save
          </button>
        </div>
      </div>
    </div>
  );
};

export default UserProfile;
