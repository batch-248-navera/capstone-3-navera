import { useContext, useState, useEffect } from "react";
import {Navigate, Link, useNavigate, useLocation} from "react-router-dom";
import { createPortal } from 'react-dom';
import AuthContext from "../store/AuthContext";
import {FaMinus,FaPlus, FaTrashAlt} from 'react-icons/fa';
import { Table } from 'flowbite-react'

const Checkout = () =>{
    
	const {user} = useContext(AuthContext);
    
	
	
	const [cartData, setCartData] = useState([]);
	const [order, setOrder] = useState([]);
	const [cartItems, setCartItems] = useState([]);
	const [qty, setQty] = useState(0)
	const [total, setTotal] = useState(0)
	const navigate = useNavigate();

	
	
	useEffect(()=>{


		fetch(`https://capstone-2-navera.onrender.com/cart/`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
					}
				})
			.then(res=> res.json())
			.then(data=>{
				setCartData(data)
				
			
			})
			.catch(error=>console.error(error));

		fetch(`https://capstone-2-navera.onrender.com/cart/userCart`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
					}
				})
			.then(res=> res.json())
			.then(data=>{
				setTotal(data.totalPrice)
				
			
			})
			.catch(error=>console.error(error));


	},[]);
	

   const placeOrder = () =>{

    
    fetch(`https://capstone-2-navera.onrender.com/users/checkout`, {
			method: "POST",
			headers: {
			  "Content-Type": "application/json",
			  Authorization: `Bearer ${localStorage.getItem("token")}`,
			},
			body: JSON.stringify({
				products: cartData,
				totalAmount: total
			}),
		  })
			.then((res) => res.json())
			.then((data) => {
				
			  if (data) {
				
				alert("Your order has been placed!");
				
			fetch(`https://capstone-2-navera.onrender.com/cart/delete`, {
				method: "DELETE",
				headers: {
				  "Content-Type": "application/json",
				  Authorization: `Bearer ${localStorage.getItem("token")}`,
				},
				
			  })
			.then(res => res.json()) 
			.then(data=>{

				
			})
			.catch(error=>console.error(error))
			
				
			  } else {
				alert("Try again!");
			  }
			});

			navigate("/");

	}

   

    const cart = cartData.map(({productId,name, quantity, price, subTotal},index)=>{
    return(
        <Table.Row key={productId} className="bg-white text-center text-black">
            
            <Table.Cell className="whitespace-nowrap font-medium text-gray-900 ">
                {name}
            </Table.Cell>
            <Table.Cell>
                ₱{price}
            </Table.Cell>
            <Table.Cell>
                {quantity}
            </Table.Cell>
            <Table.Cell>
                {subTotal}
            </Table.Cell>
            
        </Table.Row>
        );

    })
 

return(

<div>
    <div>
        <h3> Deliver Information</h3>
        <div>
            <span>{}</span>
            <span>{}</span>
            <span>Purok 1, Bariw, Camalig, South Luzon, Albay 4502</span>
        </div>
    </div>


<div className="py-4 pt-4 bg-[#f5f5f5]">
<Table hoverable={true} className="pt-20">
<Table.Head className="text-center bg-grey-500">
<Table.HeadCell>
Products Ordered
</Table.HeadCell>
<Table.HeadCell>
Unit Price
</Table.HeadCell>
<Table.HeadCell>
Quantity
</Table.HeadCell>
<Table.HeadCell>
Subtotal
</Table.HeadCell>

</Table.Head>
<Table.Body className="divide-y">
{cart}
<Table.Row  className="bg-white text-center">
            
            
            <Table.Cell/>
            <Table.Cell/>
         
            <Table.Cell>
                <span className="font-bold text-xl text-blue-500 ">Total: ₱{total}</span>
            </Table.Cell>
            <Table.Cell  className="flex text-black justify-center">
                <button  onClick={placeOrder} className="px-4 py-2 bg-black text-white">Place Order</button>
            </Table.Cell>
</Table.Row>
        
</Table.Body>
</Table>
</div>

</div>


)

}



export default Checkout;
